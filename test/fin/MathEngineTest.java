/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

import java.util.ArrayList;
import java.util.EmptyStackException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author alexxx
 */
public class MathEngineTest {
    private MathEngine instance;
    public MathEngineTest() {
        
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
        this.instance = new MathEngine();
    }
    
    @After
    public void tearDown() {
    }

     /**
     * Test of isExpression method, of class MathEngine.
     */
    @Test
    public void testIsExpression() {
        System.out.println("isExpression - not expresion");
        String line = "some random string";
        MathEngine instance = new MathEngine();
        boolean expResult = false;
        boolean result = instance.isExpression(line);
        assertEquals(expResult, result);
    }

    /**
     * Test of isExpression method, of class MathEngine.
     */
    @Test
    public void testIsExpression1() {
        System.out.println("isExpression - not valid expresion");
        String line = "some random string + asdasda +";
        MathEngine instance = new MathEngine();
        boolean expResult = false;
        boolean result = instance.isExpression(line);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isExpression method, of class MathEngine.
     */
    @Test
    public void testIsExpression2() {
        System.out.println("isExpression - expresion");
        String line = "a+b/c-6*e";
        MathEngine instance = new MathEngine();
        boolean expResult = true;
        boolean result = instance.isExpression(line);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of calculateExpression method, of class MathEngine.
     */
    @Test
    public void testCalculateExpression() throws Exception {
        System.out.println("calculateExpression: 2+2");
        String expression = "2+2";
        MathEngine instance = new MathEngine();
        String expResult = "4.0";
        String result = instance.calculateExpression(expression);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of calculateExpression method, of class MathEngine.
     */
    @Test
    public void testCalculateExpression1() throws Exception {
        System.out.println("calculateExpression: 5-6");
        String expression = "5-6";
        MathEngine instance = new MathEngine();
        String expResult = "-1.0";
        String result = instance.calculateExpression(expression);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of calculateExpression method, of class MathEngine.
     */
    @Test
    public void testCalculateExpression2() throws Exception {
        System.out.println("calculateExpression: 2+2-3");
        String expression = "2+2-3";
        MathEngine instance = new MathEngine();
        String expResult = "1.0";
        String result = instance.calculateExpression(expression);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of calculateExpression method, of class MathEngine.
     */
    @Test
    public void testCalculateExpression3() throws Exception {
        System.out.println("calculateExpression: 5-7+8");
        String expression = "5-7+8";
        MathEngine instance = new MathEngine();
        String expResult = "6.0";
        String result = instance.calculateExpression(expression);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of calculateExpression method, of class MathEngine.
     */
    @Test
    public void testCalculateExpression4() throws Exception {
        System.out.println("calculateExpression: 8-7-9");
        String expression = "8-7-9";
        MathEngine instance = new MathEngine();
        String expResult = "-8.0";
        String result = instance.calculateExpression(expression);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of calculateExpression method, of class MathEngine.
     */
    @Test
    public void testCalculateExpression5() throws Exception {
        System.out.println("calculateExpression: 2*3");
        String expression = "2*3";
        MathEngine instance = new MathEngine();
        String expResult = "6.0";
        String result = instance.calculateExpression(expression);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of calculateExpression method, of class MathEngine.
     */
    @Test
    public void testCalculateExpression6() throws Exception {
        System.out.println("calculateExpression: 5/2");
        String expression = "5/2";
        MathEngine instance = new MathEngine();
        String expResult = "2.5";
        String result = instance.calculateExpression(expression);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of calculateExpression method, of class MathEngine.
     */
    @Test
    public void testCalculateExpression7() throws Exception {
        System.out.println("calculateExpression: 2+2*2");
        String expression = "2+2*2";
        MathEngine instance = new MathEngine();
        String expResult = "6.0";
        String result = instance.calculateExpression(expression);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of calculateExpression method, of class MathEngine.
     */
    @Test
    public void testCalculateExpression8() throws Exception {
        System.out.println("calculateExpression: 5/2*2");
        String expression = "5/2*2";
        MathEngine instance = new MathEngine();
        String expResult = "5.0";
        String result = instance.calculateExpression(expression);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of calculateExpression method, of class MathEngine.
     */
    @Test
    public void testCalculateExpression9() throws Exception {
        System.out.println("calculateExpression: 5-(9+1)/2");
        String expression = "5-(9+1)/2";
        MathEngine instance = new MathEngine();
        String expResult = "0.0";
        String result = instance.calculateExpression(expression);
        assertEquals(expResult, result);
    }
     @Test
    public void testCalculateExpression10() throws Exception {
        System.out.println("calculateExpression: 12-6-3-1");
        String expression = "12-6-3-1";
        MathEngine instance = new MathEngine();
        String expResult = "2.0";
        String result = instance.calculateExpression(expression);
        assertEquals(expResult, result);
    }
      @Test
      public void testGetCloseBracetIndex() {
          String expression = "()";
          System.out.println("GetCloseBracetIndex: "+expression);
          int openBracetIndex = 0;
          int expectedBracedIndex = 1;
          int result = this.instance.getCloseBracetIndex(openBracetIndex, expression);
          assertEquals(expectedBracedIndex, result);
      }
      
      @Test
      public void testGetCloseBracetIndex_subBracet() {
          String expression = "((()))";
          System.out.println("GetCloseBracetIndex: "+expression);
          int openBracetIndex = 1;
          int expectedBracedIndex = 4;
          int result = this.instance.getCloseBracetIndex(openBracetIndex, expression);
          assertEquals(expectedBracedIndex, result);
      }
      @Test
      public void testGetCloseBracetIndex_noCloseBracet() {
          String expression = "(()";
          System.out.println("GetCloseBracetIndex: "+expression);
          int openBracetIndex = 0;
          int expectedBracedIndex = -1;
          int result = this.instance.getCloseBracetIndex(openBracetIndex, expression);
          assertEquals(expectedBracedIndex, result);
      }
      
      @Test (expected=EmptyStackException.class)
      public void testGetCloseBracetIndex_noOpenBracet() {
          String expression = ")))";
          System.out.println("GetCloseBracetIndex: " + expression);
          int openBracetIndex = 0;
          int expectedBracedIndex = -1;
          int result = this.instance.getCloseBracetIndex(openBracetIndex, expression);
      }
      
      @Test (expected=EmptyStackException.class)
      public void testGetCloseBracetIndex_wrongStartIndex() {
          String expression = "()))";
          System.out.println("GetCloseBracetIndex: "+expression);
          int openBracetIndex = 1;
          int expectedBracedIndex = -1;
          int result = this.instance.getCloseBracetIndex(openBracetIndex, expression);
      }
      
      @Test
      public void testGetNextSignIndex_plus() {
          String expression = "2+2+2";
          System.out.println("GetNextSignIndex: "+expression);
          int startIndex = 0;
          int expectedIndex = 1;
          int result = this.instance.getNextSignIndex(startIndex, expression);
          assertEquals(expectedIndex, result);
      }
      
      @Test
      public void testGetNextSignIndex_minus() {
          String expression = "2-2-2";
          System.out.println("GetNextSignIndex: "+expression);
          int startIndex = 2;
          int expectedIndex = 3;
          int result = this.instance.getNextSignIndex(startIndex, expression);
          assertEquals(expectedIndex, result);
      }
      
      @Test
      public void testGetNextSignIndex_multipy() {
          String expression = "2*2*2";
          System.out.println("GetNextSignIndex: "+expression);
          int startIndex = 2;
          int expectedIndex = 3;
          int result = this.instance.getNextSignIndex(startIndex, expression);
          assertEquals(expectedIndex, result);
      }
      
      @Test
      public void testGetNextSignIndex_divide() {
          String expression = "2/2/2";
          System.out.println("GetNextSignIndex: "+expression);
          int startIndex = 0;
          int expectedIndex = 1;
          int result = this.instance.getNextSignIndex(startIndex, expression);
          assertEquals(expectedIndex, result);
      }
      
      @Test
      public void testGetNextSignIndex_noSign() {
          String expression = "20002";
          System.out.println("GetNextSignIndex: "+expression);
          int startIndex = 0;
          int expectedIndex = 5;
          int result = this.instance.getNextSignIndex(startIndex, expression);
          assertEquals(expectedIndex, result);
      }
      
      @Test
      public void testGetNextSignIndex_wrongStartIndex() {
          String expression = "2+2";
          System.out.println("GetNextSignIndex: "+expression);
          int startIndex = 4;
          int expectedIndex = 3;
          int result = this.instance.getNextSignIndex(startIndex, expression);
          assertEquals(expectedIndex, result);
      }
      
     /* @Test
      public void testCreateInTypeExpresion_simple() {
          String expression = "2+2-4";
          System.out.println("CreateInTypeExpresion: "+expression);
          ArrayList<ExpresionElement> expected = new ArrayList<ExpresionElement>();
          Operand two = new Operand();
          two.isComplex = false;
          two.id = 0;
          two.operandText = "2";
          expected.add(two);
          Operation plus = new Operation();
          plus.id = 1;
          plus.operationText = "+";
          expected.add(plus);
          Operand two1 = new Operand();
          two1.isComplex = false;
          two1.id = 2;
          two1.operandText = "2";
          expected.add(two1);
          Operation minus = new Operation();
          minus.id = 3;
          minus.operationText = "-";
          expected.add(minus);
          Operand four = new Operand();
          four.isComplex = false;
          four.id = 4;
          four.operandText = "4";
          expected.add(four);
          ArrayList<ExpresionElement> result = this.instance.createInTypeExpresion(expression);
          assertEquals(expected, result);
      }
      */
      @Test
      public void testCreateInTypeExpresion_bracets() {
          String expression = "2+(2-4)";
          System.out.println("CreateInTypeExpresion: "+expression);
          ArrayList<ExpresionElement> expected = new ArrayList<ExpresionElement>();
          Operand two = new Operand();
          two.isComplex = false;
          two.id = 0;
          two.operandText = "2";
          expected.add(two);
          Operation plus = new Operation();
          plus.id = 1;
          plus.operationText = "+";
          expected.add(plus);
          Operand complex = new Operand();
          complex.isComplex = true;
          complex.id = 2;
          complex.operandText = "2-4";
          expected.add(complex);
          ArrayList<ExpresionElement> result = this.instance.createInTypeExpresion(expression);
          assertEquals(expected, result);
      }
      
      @Test
      public void testCreateInTypeExpresion_checkFailed() {
          String expression = "2+(2-4";
          System.out.println("CreateInTypeExpresion: "+expression);
          ArrayList<ExpresionElement> result = this.instance.createInTypeExpresion(expression);
          assertNull(result);
      }
      
      @Test
      public void testIsOperationUsed_used() {
          ArrayList<Operation> level = new ArrayList<Operation>();
          Operation one = new Operation();
          one.id = 1;
          level.add(one);
          Operation two = new Operation();
          two.id = 2;
          level.add(two);
          boolean result = this.instance.isOperationUsed(level, two);
          assertTrue(result);
      }
      
      @Test
      public void testIsOperationUsed_notUsed() {
          ArrayList<Operation> level = new ArrayList<Operation>();
          Operation one = new Operation();
          one.id = 1;
          level.add(one);
          Operation two = new Operation();
          two.id = 2;
          level.add(two);
          Operation three = new Operation();
          three.id = 3;
          boolean result = this.instance.isOperationUsed(level, three);
          assertFalse(result);
      }
      
       @Test
      public void testIsOperandUsed_used() {
          ArrayList<Operation> level = new ArrayList<Operation>();
          Operation one = new Operation();
          one.id = 1;
          one.inOperand1 = new Operand();
          one.inOperand2 = new Operand();
          level.add(one);
          Operation two = new Operation();
          two.id = 2;
          two.inOperand2 = new Operand();
          level.add(two);
          Operand operand = new Operand();
          operand.id = 3;
          operand.operandText = "1";
          operand.operationTo = two;
          two.inOperand1 = operand;
          boolean result = this.instance.isOperandUsed(level, operand);
          assertTrue(result);
      }
      
      @Test
      public void testIsOperandUsed_notUsed() {
          ArrayList<Operation> level = new ArrayList<Operation>();
          Operation one = new Operation();
          one.id = 1;          
          one.inOperand1 = new Operand();
          one.inOperand2 = new Operand();
          level.add(one);
          Operation two = new Operation();
          two.id = 2;
          two.inOperand1 = new Operand();
          two.inOperand2 = new Operand();
          level.add(two);
          Operand operand = new Operand();
          operand.id = 3;
          operand.operandText = "1";
          boolean result = this.instance.isOperandUsed(level, operand);
          assertFalse(result);
      }
      
      @Test
      public void testCreateOperations_operandsNotUsed() {
          Operation operation = new Operation();
          operation.id = 1;
          operation.operationText = "*";
          
          Operand one = new Operand();
          one.id = 2;
          one.operandText = "1";
          
          Operand two = new Operand();
          two.id = 3;
          two.operandText = "2";
          ArrayList<ExpresionElement> in = new ArrayList<ExpresionElement>();
          in.add(one);
          in.add(operation);
          in.add(two);
          ArrayList<Operation> expected = new ArrayList<Operation>();
          expected.add(operation);
          ArrayList<Operation> result = this.instance.createOperations(in);
          assertEquals(expected, result);
      }
      
      @Test
      public void testCreateOperations_operandsNotUsed_minorOperation() {
          Operation operation = new Operation();
          operation.id = 1;
          operation.operationText = "+";
          
          Operand one = new Operand();
          one.id = 2;
          one.operandText = "1";
          
          Operand two = new Operand();
          two.id = 3;
          two.operandText = "2";
          ArrayList<ExpresionElement> in = new ArrayList<ExpresionElement>();
          in.add(one);
          in.add(operation);
          in.add(two);
          ArrayList<Operation> expected = new ArrayList<Operation>();
          expected.add(operation);
          ArrayList<Operation> result = this.instance.createOperations(in);
          assertEquals(expected, result);
      }
      
      @Test
      public void testCreateOperations_operandIsUsed() {
          Operation operation = new Operation();
          operation.id = 1;
          operation.operationText = "*";
          
          Operand one = new Operand();
          one.id = 2;
          one.operandText = "1";
          
          Operand two = new Operand();
          two.id = 3;
          two.operandText = "2";
          
          Operation nextOperation = new Operation();
          nextOperation.id = 4;
          nextOperation.operationText = "*";
                    
          ArrayList<ExpresionElement> in = new ArrayList<ExpresionElement>();
          in.add(one);
          in.add(operation);
          in.add(two);
          in.add(nextOperation);
          in.add(new Operand());
          ArrayList<Operation> expected = new ArrayList<Operation>();
          expected.add(operation);
          ArrayList<Operation> result = this.instance.createOperations(in);
          assertEquals(expected, result);
      }
      
      @Test
      public void testCreateOperations_operandIsUsed_minorOperation() {
          Operation operation = new Operation();
          operation.id = 1;
          operation.operationText = "+";
          
          Operand one = new Operand();
          one.id = 2;
          one.operandText = "1";
          
          Operand two = new Operand();
          two.id = 3;
          two.operandText = "2";
          
          Operation nextOperation = new Operation();
          nextOperation.id = 4;
          nextOperation.operationText = "*";
                    
          ArrayList<ExpresionElement> in = new ArrayList<ExpresionElement>();
          in.add(one);
          in.add(operation);
          in.add(two);
          in.add(nextOperation);
          in.add(new Operand());
          ArrayList<Operation> expected = new ArrayList<Operation>();
          expected.add(nextOperation);
          ArrayList<Operation> result = this.instance.createOperations(in);
          assertEquals(expected, result);
      }
}
