/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author alexxx
 */
public class DataRowTest {
    
    public DataRowTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of compareTo method, of class DataRow.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo guid equal");
        DataRow o = new DataRow();
        o.guid = 1;
        DataRow instance = new DataRow();
        instance.guid = 1;
        int expResult = 0;
        int result = instance.compareTo(o);
        assertEquals(expResult, result);
    }

    /**
     * Test of compareTo method, of class DataRow.
     */
    @Test
    public void testCompareToByDate() {
        System.out.println("compareTo equal ByDate not equal by guid");
        DataRow o = new DataRow();
        o.guid = 1;
        o.date = "01.01.1990";
        DataRow instance = new DataRow();
        instance.guid = 0;
        instance.date = "01.01.1990";
        int expResult = -1;
        int result = instance.compareTo(o);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of compareTo method, of class DataRow.
     */
    @Test
    public void testCompareToByDate1() {
        System.out.println("compareTo equal ByDate equal by guid");
        DataRow o = new DataRow();
        o.guid = 1;
        o.date = "01.01.1990";
        DataRow instance = new DataRow();
        instance.guid = 1;
        instance.date = "01.01.1990";
        int expResult = 0;
        int result = instance.compareTo(o);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of compareTo method, of class DataRow.
     */
    @Test
    public void testCompareToByDate2() {
        System.out.println("compareTo ByDate less");
        DataRow o = new DataRow();
        o.guid = 1;
        o.date = "01.01.1990";
        DataRow instance = new DataRow();
        instance.guid = 0;
        instance.date = "01.01.1980";
        int expResult = -1;
        int result = instance.compareTo(o);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of compareTo method, of class DataRow.
     */
    @Test
    public void testCompareToByDate3() {
        System.out.println("compareTo ByDate greater");
        DataRow o = new DataRow();
        o.guid = 1;
        o.date = "01.01.1990";
        DataRow instance = new DataRow();
        instance.guid = 0;
        instance.date = "01.01.2000";
        int expResult = 1;
        int result = instance.compareTo(o);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of compareTo method, of class DataRow.
     */
    @Test
    public void testCompareToByDate4() {
        System.out.println("compareTo ByDate compared by ammount if parse error");
        DataRow o = new DataRow();
        o.guid = 1;
        o.date = "01011990";
        o.setAmount(1);
        DataRow instance = new DataRow();
        instance.guid = 0;
        instance.date = "02022000";
        instance.setAmount(1);
        int expResult = 0;
        int result = instance.compareTo(o);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of compareTo method, of class DataRow.
     */
    @Test
    public void testCompareToByAmmount() {
        System.out.println("compareTo by ammount equals");
        DataRow o = new DataRow();
        o.guid = 1;
        o.setCompareByDate(false);
        o.setAmount(1);
        DataRow instance = new DataRow();
        instance.guid = 0;
        instance.setCompareByDate(false);
        instance.setAmount(1);
        int expResult = 0;
        int result = instance.compareTo(o);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of compareTo method, of class DataRow.
     */
    @Test
    public void testCompareToByAmmount1() {
        System.out.println("compareTo by ammount less");
        DataRow o = new DataRow();
        o.guid = 1;
        o.setCompareByDate(false);
        o.setAmount(10);
        DataRow instance = new DataRow();
        instance.guid = 0;
        instance.setCompareByDate(false);
        instance.setAmount(1);
        int expResult = -1;
        int result = instance.compareTo(o);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of compareTo method, of class DataRow.
     */
    @Test
    public void testCompareToByAmmount2() {
        System.out.println("compareTo by ammount greater");
        DataRow o = new DataRow();
        o.guid = 1;
        o.setCompareByDate(false);
        o.setAmount(1);
        DataRow instance = new DataRow();
        instance.guid = 0;
        instance.setCompareByDate(false);
        instance.setAmount(10);
        int expResult = 1;
        int result = instance.compareTo(o);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class DataRow.
     */
    @Test
    public void testEquals() {
        System.out.println("not equals");
        Object o = null;
        DataRow instance = new DataRow();
        boolean expResult = false;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEquals1() {
        System.out.println("not equals by guid");
        DataRow o = new DataRow();
        o.guid = 1;
        DataRow instance = new DataRow();
        instance.guid = 2;
        boolean expResult = false;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEquals2() {
        System.out.println("equals by guid");
        DataRow o = new DataRow();
        o.guid = 1;
        DataRow instance = new DataRow();
        instance.guid = 1;
        boolean expResult = true;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);
    }

}
