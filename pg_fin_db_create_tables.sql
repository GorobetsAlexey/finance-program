﻿CREATE TABLE "Items"
(
  "ItemId" bigserial NOT NULL,
  "Amount" double precision NOT NULL,
  "CreateDate" timestamp(0) without time zone NOT NULL,
  "CheckId" bigint,
  "CurrencyId" bigint,
  CONSTRAINT "PK_Items" PRIMARY KEY ("ItemId")
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "Items"
  OWNER TO postgres;
  
CREATE TABLE "Tags"
(
  "TagId" bigserial NOT NULL,
  "TagName" text NOT NULL,
  CONSTRAINT "PK_Tags" PRIMARY KEY ("TagId" )  
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "Tags"
  OWNER TO postgres;

CREATE TABLE "ItemTags"
(
  "ItemTagId" bigserial NOT NULL,
  "ItemId" bigint NOT NULL,
  "TagId" bigint NOT NULL,
  CONSTRAINT "PK_ItemTags" PRIMARY KEY ("ItemTagId" ),
  CONSTRAINT "FK_ItemTags_Items" FOREIGN KEY ("ItemId")
      REFERENCES "Items" ("ItemId") MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "FK_ItemTags_Tags" FOREIGN KEY ("TagId")
      REFERENCES "Tags" ("TagId") MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "ItemTags"
  OWNER TO postgres;

CREATE INDEX "Items_CreateDate"
  ON "Items"
  USING btree
  ("CreateDate");

CREATE INDEX "Tags_TagName"
  ON "Tags"
  USING btree
  ("TagName");

CREATE TABLE "Check"
(
  "CheckId" bigserial NOT NULL,
  "CreateDate" timestamp(0) without time zone NOT NULL,
  "File" bytea,
  "Description" text,
  CONSTRAINT "PK_Check" PRIMARY KEY ("CheckId")
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "Check"
  OWNER TO postgres;

CREATE TABLE "Currency"
(
  "CurrencyId" bigserial NOT NULL,
  "Name" text,
  "Rate" double precision NOT NULL,
  CONSTRAINT "PK_Currency" PRIMARY KEY ("CurrencyId")
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "Currency"
  OWNER TO postgres;
  
CREATE TABLE "AccountType"
(
  "Id" bigserial NOT NULL,
  "Name" text NOT NULL,
  CONSTRAINT "PK_AccountType" PRIMARY KEY ("Id")
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "AccountType"
  OWNER TO postgres;
  
CREATE TABLE "Account"
(
  "Description" text,
  "Id" bigserial NOT NULL,
  "Currency" bigint NOT NULL,
  "AccountType" bigint NOT NULL,
  CONSTRAINT "PK_Account" PRIMARY KEY ("Id"),
  CONSTRAINT "FK_Account_AccountType" FOREIGN KEY ("AccountType")
      REFERENCES "AccountType" ("Id") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "FK_Account_Currency" FOREIGN KEY ("Currency")
      REFERENCES "Currency" ("CurrencyId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "Account"
  OWNER TO postgres;

CREATE INDEX "FKI_Account_AccountType"
  ON "Account"
  USING btree
  ("AccountType");

CREATE INDEX "FKI_Account_Currency"
  ON "Account"
  USING btree
  ("Currency");
  
 CREATE TABLE "AccountDiff"
(
  "Account" bigint NOT NULL,
  "Id" bigserial NOT NULL,
  diff double precision NOT NULL DEFAULT 0,
  before double precision NOT NULL DEFAULT 0,
  after double precision NOT NULL DEFAULT 0,
  date text,
  CONSTRAINT "PK_AccountDiff" PRIMARY KEY ("Id"),
  CONSTRAINT "FK_AccountDiff_Account" FOREIGN KEY ("Account")
      REFERENCES "Account" ("Id") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "AccountDiff"
  OWNER TO postgres;

CREATE INDEX "FKI_AccountDiff_Account"
  ON "AccountDiff"
  USING btree
  ("Account");
  
 CREATE TABLE "Target"
(
  "Currency" bigint NOT NULL,
  "Id" bigserial NOT NULL,
  ammount bigint NOT NULL DEFAULT 0,
  priority bigint NOT NULL DEFAULT 0,
  description text,
  status bigint NOT NULL DEFAULT 0,
  CONSTRAINT "PK_Target" PRIMARY KEY ("Id"),
  CONSTRAINT "FK_Target_Currency" FOREIGN KEY ("Currency")
      REFERENCES "Currency" ("CurrencyId") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "Target"
  OWNER TO postgres;

CREATE INDEX "FKI_Target_Currency"
  ON "Target"
  USING btree
  ("Currency");

 CREATE TABLE "TargetAccount"
(
  "Account" bigint NOT NULL,
  "Target" bigint NOT NULL,
  "Id" bigserial NOT NULL,
  CONSTRAINT "PK_TargetAccount" PRIMARY KEY ("Id"),
  CONSTRAINT "FK_TargetAccount_Account" FOREIGN KEY ("Account")
      REFERENCES "Account" ("Id") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "FK_TargetAccount_Target" FOREIGN KEY ("Target")
      REFERENCES "Target" ("Id") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "TargetAccount"
  OWNER TO postgres;

CREATE INDEX "FK_TargetAccount_Account"
  ON "TargetAccount"
  USING btree
  ("Account");

CREATE INDEX "FK_TargetAccount_Target"
  ON "TargetAccount"
  USING btree
  ("Target");