/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

/**
 *
 * @author alexxx
 */
public class Account {
    public String description;
    public int id;
    public AccountType type;
    public Currency currency;
    public double amount;
    
    @Override
    public String toString() {
        return description + " (" + currency.name + ")";
    }
}
