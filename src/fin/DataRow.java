/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author alexxx
 */
public class DataRow implements Comparable<DataRow>, Serializable{
    protected double _oldAmount;
    protected double _amount;
    protected boolean _isEdited = false;
    public String date;
    public String tag;
    public long guid;
    public long curency;
    private boolean compareByDate = true;
    protected Engine engine = Engine.getInstance();
    @Override
    public int compareTo(DataRow o) {
        if (this.guid == o.guid) {
            return 0;
        }
        if (compareByDate) {
            Engine engine = Engine.getInstance();
            DateFormat df = new SimpleDateFormat(engine.display_date_format);
            try {
                Date parsedDate = df.parse(this.date);
                Date oParsedDate = df.parse(o.date);
                int dateDiff = parsedDate.compareTo(oParsedDate);
                return dateDiff == 0 ? (int)(this.guid - o.guid) : dateDiff;
            } catch (ParseException ex) {}
        }
        if (this._amount == o._amount) {
            return 0;
        }
        return this._amount > o._amount ? 1 : -1;
    }
    
    @Override
    public boolean equals(Object o) {
        if(!(o instanceof DataRow)) return false;
        return this.guid == ((DataRow)o).guid;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + (int) (Double.doubleToLongBits(this._amount) ^ (Double.doubleToLongBits(this._amount) >>> 32));
        hash = 11 * hash + (this.date != null ? this.date.hashCode() : 0);
        hash = 11 * hash + (this.tag != null ? this.tag.hashCode() : 0);
        hash = 11 * hash + (int) (this.guid ^ (this.guid >>> 32));
        return hash;
    }
    
    @Override
    public String toString(){
        return this.date + "; "+this.tag+"; "+this._amount;
    }
    
    public void setCompareByDate(boolean compare) {
        this.compareByDate = compare;
    }
    
    public double getDiff() {
        if (!this._isEdited) {
            return 0;
        }
        double diff = (this._amount - this._oldAmount);        
        return engine.round(diff, 2);
    }
    
    public double getAmount() {
        return engine.round(_amount, 2);
    }
    
    public void setAmount(double newAmount) {
        this._isEdited = true;
        this._oldAmount = engine.round(_amount, 2);
        this._amount = engine.round(newAmount, 2);
    }
    
    public double getCalculateValue() {
        return -1*this._amount;
    }
    
    public void save() {        
        engine.updateDataRow(this);
        this._isEdited = false;
        CustomEvent event = new CustomEvent("DataRowEdited");
        event.throwEvent();
    }
}
