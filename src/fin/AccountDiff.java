/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author alexxx
 */
public class AccountDiff {
    public Account account;
    public int id;
    public double diff;
    public double before;
    public double after;
    public String date;
    
    public AccountDiff(Account account) {
        this.account = account;
    }
    
    public int save() throws ParseException {
        if (this.account != null) {
            Engine engine = Engine.getInstance();
            DateFormat df = new SimpleDateFormat(engine.display_date_format);
            Date parsedDate = df.parse(this.date);
            df = new SimpleDateFormat(engine.db_date_format);
            String db_date = df.format(parsedDate);
            String insert = "INSERT INTO \"AccountDiff\"(\"Account\", diff, before, after, date)" +
                " VALUES (" + this.account.id + ", " + 
                              this.diff + ", " + 
                              this.before + ", " +
                              this.after + ", " +
                              "'" + db_date + "');";
            engine.executeSQL(insert);
            String idSelect = "select max(\"Id\") as newId from \"AccountDiff\" where \"date\" = '" + db_date + "';";
            this.id = engine.getIntQuery(idSelect);
            CustomEvent event = new CustomEvent("AccountDiffSaved");
            event.throwEvent();
        }
        return -1;
    }
}
