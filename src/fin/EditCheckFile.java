/*
 * EditCheckFile.java
 *
 * Created on 03.01.2015, 21:23:46
 */
package fin;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.WebcamResolution;
import com.github.sarxos.webcam.WebcamUtils;

/**
 *
 * @author alexxx
 */
public class EditCheckFile extends javax.swing.JFrame {

    /** Creates new form EditCheckFile */
    public EditCheckFile(Check check) {
        this.check = check;
        this.engine = Engine.getInstance();
        initComponents();
        selectFile();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        selectFileBtn = new javax.swing.JButton();
        takePhotoBtn = new javax.swing.JButton();
        saveBtn = new javax.swing.JButton();
        displayScroll = new javax.swing.JScrollPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        selectFileBtn.setText("Выбрать файл");
        selectFileBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectFileBtnActionPerformed(evt);
            }
        });

        takePhotoBtn.setText("Сделать снимок");
        takePhotoBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                takePhotoBtnActionPerformed(evt);
            }
        });

        saveBtn.setText("Сохранить");
        saveBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(displayScroll, javax.swing.GroupLayout.DEFAULT_SIZE, 663, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(selectFileBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(takePhotoBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(saveBtn)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(displayScroll, javax.swing.GroupLayout.DEFAULT_SIZE, 512, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(selectFileBtn)
                    .addComponent(takePhotoBtn)
                    .addComponent(saveBtn))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void selectFileBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectFileBtnActionPerformed
        selectFile();
    }//GEN-LAST:event_selectFileBtnActionPerformed

    private void takePhotoBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_takePhotoBtnActionPerformed
        resetWebView();
    }//GEN-LAST:event_takePhotoBtnActionPerformed

    private void saveBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveBtnActionPerformed
        if (this.buffer.length > 0) {
            this.engine.updateCheckFile(this.check, this.buffer);
        } else {
            this.engine.updateCheckFile(this.check, null);
        }
        this.engine.updateCheckList();
        CustomEvent event = new CustomEvent("CheckSaved");
        event.throwEvent();
        this.setVisible(false);
    }//GEN-LAST:event_saveBtnActionPerformed

    public static void main(String[] args){
        EditCheckFile f = new EditCheckFile(null);
        f.setVisible(true);
    }
    private void resetWebView() {
        if (!this.webBtnState) {
            if (this.webcam != null) {
                this.webcam.close();
            }
            this.webcam = Webcam.getDefault();
            this.webcam.setViewSize(WebcamResolution.VGA.getSize());
            WebcamPanel panel = new WebcamPanel(this.webcam);
            panel.setFPSDisplayed(true);
            panel.setDisplayDebugInfo(true);
            panel.setImageSizeDisplayed(true);
            panel.setMirrored(true);
            this.webBtnState = true;
            displayScroll.setViewportView(panel);
            displayScroll.updateUI();
        } else {
            byte[] bytes = WebcamUtils.getImageBytes(webcam, "png");
            if (this.webcam != null) {
                this.webcam.close();
            }
            this.buffer = bytes;
            CheckFileView view = new CheckFileView(bytes);
            this.webBtnState = false;
            displayScroll.setViewportView(view);
            displayScroll.updateUI();
        }
    }
    
    private void selectFile() {
        if (this.webcam != null) {
            this.webcam.close();
        }
        this.webBtnState = false;
        this.buffer = this.engine.readImage(null);
        CheckFileView view = new CheckFileView(this.buffer);
        displayScroll.setViewportView(view);
        displayScroll.updateUI();
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane displayScroll;
    private javax.swing.JButton saveBtn;
    private javax.swing.JButton selectFileBtn;
    private javax.swing.JButton takePhotoBtn;
    // End of variables declaration//GEN-END:variables
    private Check check;
    private Engine engine;
    private byte[] buffer;
    private Webcam webcam;
    private boolean webBtnState = false;
}

