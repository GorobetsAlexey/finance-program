/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

import java.util.ArrayList;

/**
 *
 * @author alexxx
 */
public class Observer {
    
    private ArrayList<ICustomEventListener> listeners;
    private ArrayList<ICustomEventWithParamsListener> listenersWithParams;
    private String eventName;
    
    public Observer(String eventName) {
        this.listeners = new ArrayList<ICustomEventListener>(); 
        this.eventName = eventName;
    }
    
    protected void addListener(ICustomEventListener listener) {
        this.listeners.add(listener);
    }
    
    protected void addListener(ICustomEventWithParamsListener listener) {
        this.listenersWithParams.add(listener);
    }
    
    protected void processEvent() {
        for (ICustomEventListener listener: this.listeners) {
            listener.onEvent();
        }
    }
    
    protected void processEvent(Object params) {
        for (ICustomEventWithParamsListener listener: this.listenersWithParams) {
            listener.onEvent(params);
        }
        this.processEvent();
    }
}