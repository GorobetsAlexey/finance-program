/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author alexxx
 */
public class Engine {
    // JDBC driver name and database URL
    private final String JDBC_DRIVER = "org.postgresql.Driver";  
    private Connection conn = null;
    private String dataFile;
    private Double total = 0.0;
    private ArrayList<Tag> tags = new ArrayList<Tag>();
    private ArrayList<DataRow> items = new ArrayList<DataRow>();
    private ArrayList<DataRow> notAssignedItems = new ArrayList<DataRow>();
    private ArrayList<Check> checks = new ArrayList<Check>();
    private String hintKey = "";
    private String[] hints = new String[0];
    private String page_key = "";
    private int pageRowCount = 31;
    private static Engine instanse;
    private File currentDir = new File("D:\\Pictures\\tmp\\checks\\");
    private String dataRowBaseSelect = "SELECT \"ItemId\", \"Amount\", \"CreateDate\" FROM \"Items\"";
    
    public String db_date_format = "yyyy-MM-dd hh:mm:ss";
    public String display_date_format = "dd.MM.yyyy";
    public Currency currentCurrency;
    public ArrayList<Currency> currencyList;

    private Engine(String dataFile){
        this.dataFile = dataFile;
        this.initDbConnection();
        this.initCurrencyList();
    }
    
    public static Engine getInstance() {
        if (instanse == null) {
            instanse = new Engine("fin.csv");
        }
        return instanse;
    }
    
    public double GetTotal(){
        if (this.total == 0){
            try{
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT * FROM \"Items\";");
                while(rs.next()){
                    this.total += rs.getDouble("Amount");
                }
                rs.close();
                stmt.close();
            } catch(SQLException e){
                Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
                return 0;
            }
            this.total = round(this.total, 2);
        }
        return this.total;
    }

    public ArrayList<DataRow> getAllLines(int pageNumber){
        return getAllLines(this.pageRowCount, pageNumber*this.pageRowCount);
    }
    
    public ArrayList<DataRow> getAllLines(int limit, int offset){
        if(!this.items.isEmpty() && this.page_key.equals(limit+"_"+offset)) return this.items;
        ArrayList<DataRow> result = new ArrayList<DataRow>();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(this.dataRowBaseSelect + " order by \"CreateDate\" desc limit "+limit+" offset "+offset+";");       
            while(rs.next()){
             //Retrieve by column name
                DataRow element = new DataRow();
                element.date = this.formDate(rs, "CreateDate");
                element.setAmount(rs.getDouble("Amount"));
                element.guid = rs.getLong("ItemId");
                Statement getTag = conn.createStatement(); //todo join       
                ResultSet tag = getTag.executeQuery("SELECT * FROM \"Tags\" "+
                                                    " join \"ItemTags\" on \"Tags\".\"TagId\" = \"ItemTags\".\"TagId\" "+
                                                    " where \"ItemTags\".\"ItemId\" = "+element.guid+";");
                if (tag.next())
                    element.tag = tag.getString("TagName");
                tag.close();
                getTag.close();
                result.add(element);
            }
            rs.close();
            stmt.close();
        } catch(SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return null;
        } catch(ParseException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
       // Collections.sort(result, new LinesComparator());
        this.items = result;
        this.page_key = limit + "_" + offset;
        return result;
    }
    
    public ArrayList<DataRow> getAllLines(){
        if (!this.items.isEmpty() && this.page_key.isEmpty()) return this.items;
        ArrayList<DataRow> result = new ArrayList<DataRow>();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(this.dataRowBaseSelect + " order by \"CreateDate\";");    
            while(rs.next()){
             //Retrieve by column name
                DataRow element = this.parseDataRow(rs);
                result.add(element);
            }
            rs.close();
            stmt.close();
        } catch(SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return null;
        } catch(ParseException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
        Collections.sort(result, new LinesComparator());
        this.items = result;
        this.page_key = "";
        return result;
    }
    
    public ArrayList<DataRow> getAllNotAssignedLines(){
        if (!this.notAssignedItems.isEmpty()) return this.notAssignedItems;
        ArrayList<DataRow> result = new ArrayList<DataRow>();
        try{
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(this.dataRowBaseSelect + " where \"CheckId\" is null order by \"CreateDate\";");
            while(rs.next()){
             //Retrieve by column name
                DataRow element = this.parseDataRow(rs);
                result.add(element);
            }
            rs.close();
            stmt.close();
        } catch(SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return null;
        } catch(ParseException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return null;
        } 
        Collections.sort(result, new LinesComparator());
        this.notAssignedItems = result;
        return result;
    }

    public ArrayList<AccountDiff> getAccountDiffLines(Account account){
        ArrayList<AccountDiff> result = new ArrayList<AccountDiff>();
        if (account == null) {
            return result;
        }
        try{
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT \"Id\", \"diff\", \"date\", \"before\", \"after\" from \"AccountDiff\" where \"Account\" = " + account.id + " order by \"Id\" desc;");
            while(rs.next()){
             //Retrieve by column name
                AccountDiff element = this.parseAccountDiff(rs, account);
                result.add(element);
            }
            rs.close();
            stmt.close();
        } catch(SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return null;
        } catch(ParseException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
       // Collections.sort(result, new LinesComparator());
        return result;
    }
    
    public ArrayList<Target> getTargets(){
        ArrayList<Target> result = new ArrayList<Target>();
        try{
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(Target.getAllSelect());
            while(rs.next()){
             //Retrieve by column name
                Target element = new Target(rs);
                result.add(element);
            }
            rs.close();
            stmt.close();
        } catch(SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
       // Collections.sort(result, new LinesComparator());
        return result;
    }
    
    public ArrayList<TargetAccount> getTargetAccounts(Target target){
        ArrayList<TargetAccount> result = new ArrayList<TargetAccount>();
        try{
            Statement stmt = conn.createStatement();
            String select = target ==null ? TargetAccount.getAllSelect() : TargetAccount.getSelect(target);
            ResultSet rs = stmt.executeQuery(select);
            while(rs.next()){
             //Retrieve by column name
                TargetAccount element = new TargetAccount(rs);
                result.add(element);
            }
            rs.close();
            stmt.close();
        } catch(SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
       // Collections.sort(result, new LinesComparator());
        return result;
    }
    
    public void DeleteTargetAccount(TargetAccount targetAccount) {
        executeSQL(targetAccount.getDelete());
    }
   
    public void DeleteTarget(Target target) {
        executeSQL(target.getDelete());
    }
    
    public void SaveNewTargetAccount(TargetAccount targetAccount) {
        executeSQL(targetAccount.getInsert());
    }
    
    public void SaveNewTarget(Target target) {
        executeSQL(target.getInsert());
    }
    
    public double getAccountValue(Account account){
        double result = 0;
        try{
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select \"after\" from \"AccountDiff\" where \"Account\" = " + account.id + " order by \"Id\" desc limit 1;");
            while(rs.next()){
             //Retrieve by column name
                result = rs.getDouble("after");
            }
            rs.close();
            stmt.close();
        } catch(SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return 0;
        }
       // Collections.sort(result, new LinesComparator());
        return result;
    }
    
    public ArrayList<Account> getAccounts(){
        ArrayList<Account> result = new ArrayList<Account>();
        try{
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select \n" +
                "	accInfo.*, \n" +
                "	ad.after as \"Amount\"\n" +
                "from (\n" +
                "	select\n" +
                "		a.\"Id\" as \"AccountId\",\n" +
                "		a.\"Description\" as \"Description\",\n" +
                "		c.\"Name\" as \"CurrencyName\",\n" +
                "		c.\"CurrencyId\" as \"CurrencyId\",\n" +
                "		c.\"Rate\" as \"CurrencyRate\",\n" +
                "		at.\"Name\" as \"AccountTypeName\",\n" +
                "		at.\"Id\" as \"AccountTypeId\"\n" +
                "	from \n" +
                "		\"Account\" a, \n" +
                "		\"Currency\" c, \n" +
                "		\"AccountType\" at\n" +
                "	where \n" +
                "		a.\"Currency\" = c.\"CurrencyId\" and \n" +
                "		a.\"AccountType\" = at.\"Id\"\n" +
                "	) as accInfo\n" +
                "	left join \"AccountDiff\" ad on ad.\"Account\" = accInfo.\"AccountId\"\n" +
                "where ad.\"Id\" in (\n" +
                "	select max(\"Id\") from \"AccountDiff\" group by \"Account\"\n" +
                ")\n" +
                "or ad.\"Id\" is null;");
            while(rs.next()){
                Account element = this.parseAccount(rs);
                result.add(element);
            }
            rs.close();
            stmt.close();
        } catch(SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
       // Collections.sort(result, new LinesComparator());
        return result;
    }
    
    public boolean Addline(String date, String tag, double amount){
        try{
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("Insert into \"Items\"(\"Amount\", \"CreateDate\") VALUES ("+amount+", '"+date+" 00:00:00');");
            ResultSet rs = stmt.executeQuery("Select * from \"Tags\" where \"TagName\" = '"+tag+"';");
            long tagId = 0;
            if (rs.next()){
                tagId = rs.getLong("TagId");
            } else {
                rs.close();
                stmt.executeUpdate("Insert into \"Tags\"(\"TagName\") VALUES ('"+tag+"');");
                rs = stmt.executeQuery("Select * from \"Tags\" where \"TagName\" = '"+tag+"';");
                if (rs.next())
                    tagId = rs.getLong("TagId");
            }
            rs.close();
            long itemId = 0;
            rs = stmt.executeQuery("select * from \"Items\"  order by \"ItemId\" desc limit 1;");
            if (rs.next()){
                itemId = rs.getLong("ItemId");
            }
            rs.close();
            stmt.executeUpdate("Insert into \"ItemTags\"(\"ItemId\", \"TagId\") VALUES ("+itemId+", "+tagId+");"); 
                        
            this.total += amount;
            this.total = round(this.total, 2);
            if(this.tags.indexOf(new Tag(tag)) < 0)
                this.tags.add(new Tag(tag));
            DataRow newLine = new DataRow();
            newLine.date = date;
            newLine.tag = tag;
            newLine.setAmount(amount);
            newLine.guid = itemId;
            this.items.add(0, newLine);
            this.notAssignedItems.add(0, newLine);
        } catch (SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
        return true;
    }
    
    public double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public String[] initTags(){
        ArrayList<Tag> result = new ArrayList<Tag>();
        if(this.tags.isEmpty()){
            try{
                Statement stmt = conn.createStatement();
                Statement stmt1 = conn.createStatement();
                ResultSet rs = stmt.executeQuery("Select \"TagId\", \"TagName\" from \"Tags\"");
                while(rs.next()){
                    Tag tag = new Tag(rs.getString("TagName"));
                    ResultSet useCount = stmt1.executeQuery("SELECT COUNT(*) AS COUNT FROM \"Items\" "+
                                                           " join \"ItemTags\" on \"Items\".\"ItemId\" = \"ItemTags\".\"ItemId\" where \"ItemTags\".\"TagId\" = "+
                                                            rs.getLong("TagId"));
                    if(useCount.next())
                        tag.useCount = useCount.getInt("count");
                    useCount.close();
                    result.add(tag);
                }
                rs.close();  
                stmt.close();
                stmt1.close();
                this.tags.addAll(result);
            } catch (SQLException e){
                Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
                return null;
            }
        } else {
            result.addAll(this.tags);
        }
        Collections.sort(result);
        String[] ret = new String[result.size()];
        int index = 0;
        for (Tag tag : result) {
            ret[index] = tag.tag;
            index++;
        }
        return ret;
    }
    
    public String[] getHints(String starts){
        if (hintKey.equals(starts)) return this.hints;
        if(starts.isEmpty()) {this.hintKey = ""; this.hints = new String[0]; return this.hints;}
        this.hintKey = starts;
        this.hints = new String[5];
        int index = 0;
        
        try{
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from \"Tags\" where left(\"TagName\", "+starts.length()+") = '"+starts+"' limit 5");
            while(rs.next()){
                this.hints[index] = rs.getString("TagName");
                index++;
            }
            rs.close();  
            stmt.close();        
        } catch (SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
        this.hints = Arrays.copyOfRange(this.hints, 0, index);
        return this.hints;
    }
    
    public ArrayList<DataRow> getLinesByFilter(String filter){
        ArrayList<DataRow> result = new ArrayList<DataRow>();
        if (this.items.isEmpty()) getAllLines();        
        for (DataRow element : this.items){
            if (element.tag.contains(filter)){
                result.add(element);
            }
        }
        Collections.sort(result, new LinesComparator());
        return result;
    }
    
    public void clearItems() {
        this.items.clear();
    }
    
    public ArrayList<DataRow> getLinesByFilter(String filter, int pageNumber){
        return getLinesByFilter(filter, this.pageRowCount, pageNumber*this.pageRowCount);
    }
    
    public ArrayList<DataRow> getLinesByFilter(String filter, int limit, int offset){
        return getLinesByFilter(filter, limit, offset, "", "");
    }
    
    public ArrayList<DataRow> getLinesByFilter(String filter,String fromDate, String toDate){
        return getLinesByFilter(filter, -1, -1, fromDate, toDate);
    }
    
    public ArrayList<DataRow> getLinesByFilter(String filter, int limit, int offset,
            String fromDate, String toDate){
        ArrayList<DataRow> result = new ArrayList<DataRow>();
        try{
            Statement stmt = conn.createStatement();
            String sql = "SELECT \"ItemId\", \"Amount\", \"CreateDate\" FROM \"Items\" ";
            ArrayList<String> conditions = new ArrayList<String>();
            if (!filter.isEmpty()) {
                conditions.add("\"ItemId\" in (select \"ItemId\" from \"ItemTags\" where \"TagId\" in (select \"TagId\" from \"Tags\" where \"TagName\" like '"+filter+"%'))");
            }
            if (!fromDate.isEmpty()) {
                conditions.add("\"CreateDate\" >= '" + fromDate + "'");
            }
            if (!toDate.isEmpty()) {
                conditions.add("\"CreateDate\" <= '" + toDate + "'");
            }
            if (!conditions.isEmpty()) {
                sql += " where ";
            }
            Iterator<String> iterator = conditions.iterator();
            while(iterator.hasNext()) {
                sql += " " + iterator.next();
                if(iterator.hasNext()) {
                    sql += " and ";
                }
            }
            sql += " order by \"CreateDate\" desc";
            if (limit > 0) {
                sql += " limit " + limit;
            }
            if (offset >= 0) {
                sql += " offset " + offset;
            }
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
             //Retrieve by column name
                DataRow element = new DataRow();
                element.date = this.formDate(rs, "CreateDate");
                element.setAmount(rs.getDouble("Amount"));
                element.guid = rs.getLong("ItemId");
                Statement getTag = conn.createStatement(); //todo join       
                ResultSet tag = getTag.executeQuery("SELECT * FROM \"Tags\" "+
                                                    " join \"ItemTags\" on \"Tags\".\"TagId\" = \"ItemTags\".\"TagId\" "+
                                                    " where \"ItemTags\".\"ItemId\" = "+element.guid+";");
                if (tag.next())
                    element.tag = tag.getString("TagName");
                tag.close();
                getTag.close();
                result.add(element);
            }
            rs.close();
            stmt.close();
        } catch(SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return null;
        } catch(ParseException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
        return result;
    }

    public boolean updateDataRow(DataRow row){
        try{
            Statement stmt = conn.createStatement();            
            ResultSet rs = stmt.executeQuery("Select * from \"Tags\" where \"TagName\" = '"+row.tag+"';");
            long tagId = 0;
            if (rs.next()){
                tagId = rs.getLong("TagId");
            } else {
                rs.close();
                stmt.executeUpdate("Insert into \"Tags\"(\"TagName\") VALUES ('"+row.tag+"');");
                rs = stmt.executeQuery("Select * from \"Tags\" where \"TagName\" = '"+row.tag+"';");
                if (rs.next())
                    tagId = rs.getLong("TagId");
            }
            rs.close();
            stmt.executeUpdate("UPDATE \"Items\" SET \"Amount\"="+row.getAmount()+
                               ", \"CreateDate\"='"+row.date+" 00:00:00' "+
                               " WHERE \"ItemId\" = "+row.guid + ";");
            stmt.executeUpdate("DELETE FROM \"ItemTags\" WHERE \"ItemId\" = "+row.guid);
            stmt.executeUpdate("Insert into \"ItemTags\"(\"ItemId\", \"TagId\") VALUES ("+row.guid+", "+tagId+");");
            stmt.close();
        } catch (SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
        this.total += row.getDiff();
        this.total = round(this.total, 2);
        this.items.set(this.items.indexOf(row), row);
        if (this.notAssignedItems.indexOf(row) >= 0) {
            this.notAssignedItems.set(this.notAssignedItems.indexOf(row), row);
        }
        return true;
    }
    
    public void RemoveFromNotAssigned(DataRow row) {
        if (this.notAssignedItems.indexOf(row) >= 0){
            this.notAssignedItems.remove(row);
        }
    }
    
    public void RemoveFromAssigned(DataRow row) {
        if (this.notAssignedItems.indexOf(row) < 0){
            this.notAssignedItems.add(row);
            Collections.sort(this.notAssignedItems, new LinesComparator());
        }
    }

    public void ResetNotAssignedRows(ArrayList<DataRow> rows) {
        this.notAssignedItems.addAll(rows);
        Collections.sort(this.notAssignedItems, new LinesComparator());
    }
    
    public boolean UpdateCheckIdForRows(ArrayList<DataRow> rows, long checkId) {
        try{
            Statement stmt = conn.createStatement();
            String sql = "UPDATE \"Items\" SET \"CheckId\"="+checkId+
                    " WHERE \"ItemId\" in (";
            for (DataRow row : rows) {
                this.RemoveFromNotAssigned(row);
                sql += row.guid + ",";
            }
            sql = sql.substring(0, sql.length()-1);
            sql += ");";
            stmt.executeUpdate(sql);
        } catch (SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
        return true;
    }
    
    private Account parseAccount(ResultSet rs) throws SQLException {
        Account element = new Account();        
        AccountType type = new AccountType();
        element.description = rs.getString("Description");
        element.id = rs.getInt("AccountId");
        element.amount = rs.getDouble("Amount");        
        element.currency = this.parseCurrency(rs);
        type.id = rs.getInt("AccountTypeId");
        type.name = rs.getString("AccountTypeName");
        element.type = type;
        return element;
    }
    
    private Currency parseCurrency(ResultSet rs) throws SQLException {
        Currency currency = new Currency();
        currency.id = rs.getInt("CurrencyId");
        currency.name = rs.getString("CurrencyName");
        currency.rate = rs.getDouble("CurrencyRate");
        return currency;
    }
    
    private AccountDiff parseAccountDiff(ResultSet rs, Account account) throws SQLException, ParseException {
        AccountDiff element = new AccountDiff(account);
        element.date = this.formDate(rs, "date");
        element.id = rs.getInt("Id");
        element.before = rs.getDouble("before");
        element.after = rs.getDouble("after");
        element.diff = rs.getDouble("diff");
        return element;
    }

    private DataRow parseDataRow(ResultSet rs) throws SQLException, ParseException {
        DataRow element = new DataRow();
        element.date = this.formDate(rs, "CreateDate");
        element.setAmount(rs.getDouble("Amount"));
        element.guid = rs.getLong("ItemId");
        Statement getTag = conn.createStatement(); //todo join       
        ResultSet tag = getTag.executeQuery("SELECT * FROM \"Tags\" "+
                                            " join \"ItemTags\" on \"Tags\".\"TagId\" = \"ItemTags\".\"TagId\" "+
                                            " where \"ItemTags\".\"ItemId\" = "+element.guid+";");
        if (tag.next())
            element.tag = tag.getString("TagName");
        tag.close();        
        getTag.close();
        return element;
    }
    
    private String formDate(ResultSet rs, String columnName) throws SQLException, ParseException {
        String rawData = rs.getString(columnName);
        DateFormat df = new SimpleDateFormat(this.db_date_format);
        Date parsedDate = df.parse(rawData);
        df = new SimpleDateFormat(this.display_date_format);
        return df.format(parsedDate);
    }
    
    private void initCurrencyList() {
        this.currencyList = new ArrayList<Currency>();
        try{
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(
                "	select\n" +
                "		\"Name\" as \"CurrencyName\",\n" +
                "		\"CurrencyId\" as \"CurrencyId\",\n" +
                "		\"Rate\" as \"CurrencyRate\"\n" +
                "	from \n" +
                "		\"Currency\";");
            while(rs.next()){
                Currency element = this.parseCurrency(rs);
                this.currencyList.add(element);
            }
            rs.close();
            stmt.close();
        } catch(SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private boolean initDbConnection(){
        try{
            File connectionString = new File("ConnectionString");
            FileInputStream f = new FileInputStream(connectionString);
            DataInputStream dis = new DataInputStream(f);
            BufferedReader _in = new BufferedReader(new InputStreamReader(dis, "Cp1251"));
            String line = _in.readLine();
            String rawUrl = line.split(";")[0];
            String rawUser = line.split(";")[1];
            String rawPassword = line.split(";")[2];
            _in.close();
            dis.close();
            f.close();
            String url = rawUrl.split("=")[1];
            String user = rawUser.split("=")[1];
            String password = rawPassword.split("=")[1];
            //Register JDBC driver
            Class.forName(JDBC_DRIVER);
            //Open a connection
            conn = DriverManager.getConnection(url,user,password);
        }catch (SQLException se){
          //Handle errors for JDBC          
          Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, se);
          return false;
        }catch(Exception e){
          //Handle errors for Class.forName
          Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
          return false;
        }
        return true;
    }
    
    public void closeDbConnection(){
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void initQuery(){
        ArrayList<DataRow> result = new ArrayList<DataRow>();
        try{
            FileInputStream f = new FileInputStream(this.dataFile);
            DataInputStream dous = new DataInputStream(f);
            BufferedReader _in = new BufferedReader(new InputStreamReader(dous, "Cp1251"));

            String line = _in.readLine();
            while(line != null && !line.isEmpty()){                
                DataRow element = new DataRow();
                element.date = line.split(";")[0];                
                element.tag = line.split(";")[1];
                element.setAmount(Double.parseDouble(line.split(";")[2].replace(",", ".")));
                //TODO init quid with ItemId
                result.add(element);
                line = _in.readLine();
            }
            _in.close();
            dous.close();
            f.close();
        } catch (IOException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return;
        }
        Collections.sort(result, new LinesComparator());
        try{
        Statement stmt = conn.createStatement();
        for(DataRow curr : result){
            stmt.executeUpdate("Insert into \"Items\"(\"Amount\", \"CreateDate\") VALUES ("+curr.getAmount()+", '"+curr.date+" 00:00:00');");
            ResultSet rs = stmt.executeQuery("Select * from \"Tags\" where \"TagName\" = '"+curr.tag+"';");
            long tagId = 0;
            if (rs.next()){
                tagId = rs.getLong("TagId");
            } else {
                rs.close();
                stmt.executeUpdate("Insert into \"Tags\"(\"TagName\") VALUES ('"+curr.tag+"');");
                rs = stmt.executeQuery("Select * from \"Tags\" where \"TagName\" = '"+curr.tag+"';");
                if (rs.next())
                    tagId = rs.getLong("TagId");
            }
            rs.close();
            long itemId = 0;
            rs = stmt.executeQuery("select * from \"Items\"  order by \"ItemId\" desc limit 1;");
            if (rs.next()){
                itemId = rs.getLong("ItemId");
            }
            rs.close();
            stmt.executeUpdate("Insert into \"ItemTags\"(\"ItemId\", \"TagId\") VALUES ("+itemId+", "+tagId+");");            
        }
        stmt.close();
        } catch (SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return;
        }        
    }
    
    public void saveQuery(){
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT \"Amount\", \"CreateDate\", \"TagName\" FROM \"Items\" "+
                                     "join \"ItemTags\" on \"ItemTags\".\"ItemId\" = \"Items\".\"ItemId\" "+
                                     "join \"Tags\" on \"Tags\".\"TagId\" = \"ItemTags\".\"TagId\" order by \"CreateDate\"");
            FileOutputStream fs = new FileOutputStream(this.dataFile, true);
            DataOutputStream dous = new DataOutputStream(fs);
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(dous, "Cp1251"));
            while(rs.next()){
                DataRow element = new DataRow();
                element.date = this.formDate(rs, "CreateDate");
                element.setAmount(rs.getDouble("Amount"));
                element.tag = rs.getString("TagName");
                String line = element.toString() + "\n";
                out.write(line);
            }
            out.close();
            dous.close();
            fs.close();
            rs.close();  
            stmt.close();        
        } catch (SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException ex){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        } catch(ParseException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public long AddCheck(String date){
        try{
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("Insert into \"Check\"(\"CreateDate\") VALUES ('"+date+" 00:00:00');");
            Statement readstmt = conn.createStatement();
            ResultSet rs = readstmt.executeQuery("SELECT \"CheckId\" FROM \"Check\" "+
                                     "order by \"CheckId\" desc limit 1 ");
            if (rs.next()) {
                return rs.getLong("CheckId");
            } else {
                return -1;
            }
        } catch (SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return -1;
        }
    }
    
    public void UpdateCheck(Check check){
        try{
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("Update \"Check\" set \"CreateDate\" = '"+check.date + " 00:00:00', " +
                    "\"Description\" = '"+check.description+"' where \"CheckId\" = "+ check.guid);
           
        } catch (SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void executeSQL(String query){
        try{
            Statement stmt = conn.createStatement();
            stmt.execute(query);           
        } catch (SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public int getIntQuery(String query){
        try{
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            rs.next();
            int result = rs.getInt(1);
            rs.close();
            stmt.close();
            return result;
        } catch (SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return -1;
        }
    }
    
    public int getCheckCount() {
        if (this.checks.isEmpty()) {
            this.getChecks();
        }
        return this.checks.size();
    }
    
    public ArrayList<Check> getChecks() {
        if (!this.checks.isEmpty()) return this.checks;
        ArrayList<Check> result = new ArrayList<Check>();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select \"Check\".\"CheckId\", \"Check\".\"CreateDate\", \"Check\".\"Description\", " +
                        "SUM(\"Items\".\"Amount\") as \"Amount\", \"Check\".\"File\" from \"Check\" "+
                        "join \"Items\" on \"Check\".\"CheckId\" = \"Items\".\"CheckId\" group by \"Check\".\"CheckId\";");    
            while(rs.next()){
                Check element = new Check();
                element.date = this.formDate(rs, "CreateDate");
                element.description = rs.getString("Description");
                element.amount = rs.getDouble("Amount");
                element.amount = round(element.amount, 2);
                element.guid = rs.getLong("CheckId");
                element.file = rs.getBytes("File");
                result.add(element);
            }
            rs.close();
            stmt.close();
        } catch(SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return null;
        } catch(ParseException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
        Collections.sort(result, new ChecksComparator());
        this.checks = result;
        return result;
    }

    public Check getCheck(int index) {
        if (this.checks.isEmpty()) {
            this.getChecks();
        }
        return this.checks.get(index);
    }
    
    public Check getCheckByGuid(long guid) {
        if (this.checks.isEmpty()) {
            this.getChecks();
        }
        for (Check check: this.checks) {
            if (check.guid == guid) {
                return check;
            }
        }
        return null;
    }
    
    public ArrayList<DataRow> getItemsByCheck(Check check) {
        ArrayList<DataRow> result = new ArrayList<DataRow>();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT \"ItemId\", \"Amount\", \"CreateDate\" FROM \"Items\" where \"CheckId\" = "+check.guid+";");
            while(rs.next()){
                DataRow element = new DataRow();
                element.date = this.formDate(rs, "CreateDate");
                double amount = rs.getDouble("Amount");
                element.setAmount(round(amount, 2));
                element.guid = rs.getLong("ItemId");
                Statement getTag = conn.createStatement(); //todo join       
                ResultSet tag = getTag.executeQuery("SELECT * FROM \"Tags\" "+
                                                    " join \"ItemTags\" on \"Tags\".\"TagId\" = \"ItemTags\".\"TagId\" "+
                                                    " where \"ItemTags\".\"ItemId\" = "+element.guid+";");
                if (tag.next())
                    element.tag = tag.getString("TagName");
                tag.close();
                getTag.close();
                result.add(element);
            }
            rs.close();
            stmt.close();
        } catch(SQLException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return null;
        } catch(ParseException e){
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
        Collections.sort(result, new LinesComparator());
        return result;
    }
    
    public String getFilePath() {
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "Images", "jpg", "png", "gif", "jpeg");
        chooser.setFileFilter(filter);
        chooser.setCurrentDirectory(this.currentDir);
        int result = chooser.showOpenDialog(null);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = chooser.getSelectedFile();
            this.currentDir = new File(selectedFile.getParent());
            String path = selectedFile.getAbsolutePath();
            return path;
        }
        return "";
    }
    
    public byte[] readImage(Check check) {
        String path = getFilePath();
        File file = new File(path);
        byte []buffer = new byte[(int) file.length()];
        InputStream ios = null;
        try {
            ios = new FileInputStream(file);
            ios.read(buffer);
        } catch(FileNotFoundException ex) {
        } catch(IOException ex){
        } finally { 
            try {
                if ( ios != null ) 
                ios.close();
            } catch ( IOException e) {
            }
        }
        if (check != null) {
            updateCheckFile(check, buffer);
        } 
        return buffer;
    }
    
    public void updateCheckFile(Check check, byte[] buffer) {
        try {
            PreparedStatement ps = conn.prepareStatement("Update \"Check\" set \"File\" = ? WHERE \"CheckId\" = "+check.guid+";");
            ps.setBytes(1, buffer);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateCheckList() {
        this.checks.clear();
        this.getChecks();
    }
}

class LinesComparator implements Comparator {
   
    @Override
    public boolean equals(Object obj){
        return false;
    }

    @Override
    public int compare(Object o1, Object o2) {
        String[] date1 = ((DataRow)o1).date.split("\\.");
        String[] date2 = ((DataRow)o2).date.split("\\.");
        
        if (Integer.parseInt(date2[2]) - Integer.parseInt(date1[2]) == 0){
            if (Integer.parseInt(date2[1]) - Integer.parseInt(date1[1]) == 0){
                if (Integer.parseInt(date2[0]) - Integer.parseInt(date1[0]) == 0){
                    return 0;
                }
                else return Integer.parseInt(date2[0]) - Integer.parseInt(date1[0]);
            }
            else return Integer.parseInt(date2[1]) - Integer.parseInt(date1[1]);
        }
        else return Integer.parseInt(date2[2]) - Integer.parseInt(date1[2]);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }
}

class ChecksComparator implements Comparator {
   
    @Override
    public boolean equals(Object obj){
        return false;
    }

    @Override
    public int compare(Object o1, Object o2) {
        String[] date1 = ((Check)o1).date.split("\\.");
        String[] date2 = ((Check)o2).date.split("\\.");
        
        if (Integer.parseInt(date2[2]) - Integer.parseInt(date1[2]) == 0){
            if (Integer.parseInt(date2[1]) - Integer.parseInt(date1[1]) == 0){
                if (Integer.parseInt(date2[0]) - Integer.parseInt(date1[0]) == 0){
                    return 0;
                }
                else return Integer.parseInt(date2[0]) - Integer.parseInt(date1[0]);
            }
            else return Integer.parseInt(date2[1]) - Integer.parseInt(date1[1]);
        }
        else return Integer.parseInt(date2[2]) - Integer.parseInt(date1[2]);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }
}