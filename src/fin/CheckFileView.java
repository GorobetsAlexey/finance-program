/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JComponent;

/**
 *
 * @author alexxx
 */
public class CheckFileView extends JComponent {
    private Image image;
    public int width = 550;
    public int height = 500;
    public double imageWidth = 550;
    public double imageHeight = 500;
    public boolean scale = true;
    private double scaleWidth;
    private double scaleHeigth;
    
    public CheckFileView(byte[] data){
        try{
            if (data != null && data.length > 0) {
                ByteArrayInputStream imgStream = new ByteArrayInputStream(data);
                image = ImageIO.read(imgStream);
            } else {
                File file = new File("");
                this.image = ImageIO.read(file);
            }
            this.imageWidth = image.getWidth(this);
            this.imageHeight = image.getHeight(this);
            double imageScaleWidth = (double)this.width / this.imageWidth;
            double imageScaleHeigth = (double)this.height / this.imageHeight;
            this.scaleWidth = Math.min(imageScaleWidth, imageScaleHeigth);
            this.scaleHeigth = Math.min(imageScaleWidth, imageScaleHeigth);
            this.setPreferredSize(new Dimension(this.width, this.height));
        }
        catch (IOException e){
        }
    }
    
    public CheckFileView(BufferedImage image){
        this.image = image;
        this.imageWidth = image.getWidth(this);
        this.imageHeight = image.getHeight(this);
        double imageScaleWidth = (double)this.width / this.imageWidth;
        double imageScaleHeigth = (double)this.height / this.imageHeight;
        this.scaleWidth = Math.min(imageScaleWidth, imageScaleHeigth);
        this.scaleHeigth = Math.min(imageScaleWidth, imageScaleHeigth);
        this.setPreferredSize(new Dimension(this.width, this.height));
    }
    
    @Override
    public void paintComponent (Graphics g){
        Graphics2D innerGraphics = (Graphics2D)g;
        this.paintComponent(innerGraphics);
    }
    
    public void paintComponent (Graphics2D g){
        if(image == null) return;
        if (this.scale) {
            g.scale(this.scaleWidth, this.scaleHeigth);
        }
        g.drawImage(image, 0, 0, null);
    }
    
    public void toggleScale() {
        this.scale = !this.scale;
        Dimension d = new Dimension(this.width, this.height);
        if (!scale) {
            d = new Dimension((int)this.imageWidth, (int)this.imageHeight);
        }
        this.setSize(d);
        this.setPreferredSize(d);
        this.updateUI();
    }
}
