/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author alexxx
 */
public class DrawableDataRow extends DataRow {
    
    public int x;
    public int y;
    public int radius = 3;
    public int selectRadius = 6;
    public int lineHeigth = 20;
    
    public DrawableDataRow(DataRow row) {
        this._amount = row.getAmount();
        this.date = row.date;
        this.tag = row.tag;
        this.guid = row.guid;
        this.curency = row.curency;
    }
    
    public void draw(Graphics g) {
        g.setColor(Color.BLUE);
        g.fillOval(this.x-this.radius, this.y-this.radius, this.radius*2, this.radius*2);
    }
    
    public boolean setSelected(int x, int y, Graphics g) {
        if(Math.abs(this.x + this.radius - x) > this.selectRadius || Math.abs(this.y + this.radius - y) > this.selectRadius){
            return false;
        }
        g.setColor(Color.BLUE);
        g.drawString(this.date, this.x+this.radius*2, this.y + this.lineHeigth);
        g.drawString(this._amount + "", this.x+this.radius*2, this.y + this.lineHeigth * 2);
        g.drawString(this.tag, this.x+this.radius*2, this.y + this.lineHeigth * 3);
        return true;
    }
    
    public void setNotSelected (Graphics g) {
        g.setColor(Color.WHITE);
        g.drawString(this.date, this.x+this.radius*2, this.y + this.lineHeigth);
        g.drawString(this._amount + "", this.x+this.radius*2, this.y + this.lineHeigth * 2);
        g.drawString(this.tag, this.x+this.radius*2, this.y + this.lineHeigth * 3);
    }
}
