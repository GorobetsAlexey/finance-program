/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

/**
 *
 * @author alexxx
 */
public class Currency {
    public int id;
    public String name;
    public double rate;
    public boolean primary;
    
    /**
     * All currency rates will be calculated relative to the base currency.
     * Base currency allways has rate 1.0 and flaged as primary.
     * @param currency
     * @return 
     */
    public double getRelativeCurrencyRate(Currency currency) {
        return currency.rate / this.rate;
    }
    
    @Override
    public String toString() {
        return this.name;
    }
    
    public void save() {
        Engine engine = Engine.getInstance();
        if (this.id > -1 ) {
            String update = "UPDATE \"Currency\" SET\n"
                    + "\"Name\" = '" + this.name + "'\n,"
                    + "\"Rate\" = '" + this.rate + "'\n"
                    + "WHERE \"CurrencyId\" = " + this.id + ";";
            engine.executeSQL(update);
            CustomEvent event = new CustomEvent("CurrencySaved");
            event.throwEvent();
        }
    }
}
