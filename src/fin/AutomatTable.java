/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

import java.util.ArrayList;

/**
 *
 * @author alexxx
 */
public class AutomatTable {
    private ArrayList<ArrayList<Integer>> table;
    public AutomatTable () {//todo consts
        this.table = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < 7; i++) {
            this.table.add(new ArrayList<Integer>());
            for (int j = 0; j < 8; j++)
               this.table.get(i).add(0); 
        }
        //sign symbol
        ArrayList<Integer> line = createDefaultLine(this.table.get(0), 0);
        line.set(0, Automat.SIGN_ERR);
        line.set(3, Automat.SIGN_ERR);
        line.set(5, Automat.FLOAT_NUMBER_FORMAT_ERR);
        line.set(6, Automat.SIGN_ERR);
        //minus
        line = createDefaultLine(this.table.get(1), 6);
        line.set(5, Automat.FLOAT_NUMBER_FORMAT_ERR);
        line.set(6, Automat.SIGN_ERR);
        //number
        line = createDefaultLine(this.table.get(2), 1);
        line.set(2, 2);
        line.set(4, Automat.SIGN_ERR);
        line.set(5, 7);
        line.set(7, 7);
        //leter
        line = createDefaultLine(this.table.get(3), 2);
        line.set(1, Automat.NUMBER_FORMAT_ERR);
        line.set(4, Automat.SIGN_ERR);
        line.set(5, Automat.FLOAT_NUMBER_FORMAT_ERR);
        line.set(7, Automat.FLOAT_NUMBER_FORMAT_ERR);
        //open bracket
        line = createDefaultLine(this.table.get(4), 3);
        line.set(1, Automat.NUMBER_FORMAT_ERR);
        line.set(2, Automat.VARIABLE_NAME_FORMAT_ERR);
        line.set(4, Automat.SIGN_ERR);
        line.set(5, Automat.FLOAT_NUMBER_FORMAT_ERR);
        line.set(7, Automat.SIGN_ERR);
        //close bracket
        line = createDefaultLine(this.table.get(5), 4);
        line.set(0, Automat.BRACKET_ERR);
        line.set(3, Automat.BRACKET_ERR);
        line.set(5, Automat.FLOAT_NUMBER_FORMAT_ERR);
        line.set(6, Automat.BRACKET_ERR);
        //point
        line = createDefaultLine(this.table.get(6), Automat.FLOAT_NUMBER_FORMAT_ERR);
        line.set(1, 5);
        line.set(2, Automat.VARIABLE_NAME_FORMAT_ERR);
    }
    
    private ArrayList<Integer> createDefaultLine(ArrayList<Integer> line, int defValue) {
        line.set(0, defValue);
        line.set(1, defValue);
        line.set(2, defValue);
        line.set(3, defValue);
        line.set(4, defValue);
        line.set(5, defValue);
        line.set(6, defValue);
        line.set(7, defValue);
        return line;
    }
    
    public int getNextState (int curState, int symbolType){        
        return this.table.get(symbolType).get(curState);
    }
}
