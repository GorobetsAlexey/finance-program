/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

/**
 *
 * @author alexxx
 */
public class Operation extends ExpresionElement{
    public int x;
    public int y;
    public Operand inOperand1;
    public Operand inOperand2;
    public Operand outOperand;
    public String operationText;

    @Override
    public int getType() {
        return ExpresionElement.operation;
    }
    
    public boolean isReadyToCalculate() {
        return (this.inOperand1.calculated() && this.inOperand2.calculated());
    }
    
    public String calculate() {
        if (this.isReadyToCalculate()) {
            double operand1 =  Double.parseDouble(this.inOperand1.calculatedValue);
            double operand2 =  Double.parseDouble(this.inOperand2.calculatedValue);
            if (this.operationText.equals("+")) {
                return (operand1 + operand2) + "";
            } else 
                if (this.operationText.equals("-")) {
                    return (operand1 - operand2) + "";
                } else 
                    if (this.operationText.equals("*")) {
                        return (operand1 * operand2) + "";
                    } else 
                        if (this.operationText.equals("/")) {
                            return (operand1 / operand2) + "";
                        }
        }
        return "";
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != this.getClass()) 
            return false;
        return ((Operation)obj).operationText.equals(this.operationText);
    }
}
