/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author alexxx
 */
public class Target {
    
    public int id;
    
    public int priority;
    
    public int ammount;
    
    public int doneAmmount;
    
    public String description;
    
    public Currency currency;
    
    public int state;
    
    public Target() {}
    
    public Target(ResultSet rs) throws SQLException {
        this.id = rs.getInt("Id");
        this.ammount = rs.getInt("ammount");
        this.priority = rs.getInt("priority");
        this.description = rs.getString("description");
        this.state = rs.getInt("status");
        Currency currency = new Currency();
        currency.id = rs.getInt("CurrencyId");
        currency.name = rs.getString("Name");
        currency.rate = rs.getDouble("Rate");
        this.currency = currency;
    }

    public static String getAllSelect() {
        return "select t.\"Id\", t.ammount, t.priority, t.description, t.status, c.\"CurrencyId\", c.\"Name\", c.\"Rate\"\n" +
                "from \"Target\" as t inner join \"Currency\" c on t.\"Currency\" = c.\"CurrencyId\" order by t.priority;";
    }
    
    public String getDelete() {
         return "DELETE FROM \"TargetAccount\" WHERE \"Target\" = "+ this.id + ";" +
                 "DELETE FROM \"Target\" WHERE \"Id\" = "+ this.id;
    }
    
    public String getInsert() {
        return "INSERT INTO \"Target\" (\"Currency\", \"ammount\", \"priority\", \"description\", \"status\") values ("
                + this.currency.id + "," + this.ammount + "," + this.priority + ",'" + this.description + "'," + this.state + ");";
    }
}
