/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

/**
 *
 * @author alexxx
 */
public class Operand extends ExpresionElement{
    public Operation operationFrom;
    public Operation operationTo;
    public String operandText;
    public boolean isComplex;    
    public String calculatedValue;
    private boolean calculated = false;
    
    @Override
    public int getType() {
        return ExpresionElement.operand;
    }
    
    public boolean calculated() {
        if (this.calculated) {
            return true;
        }
        if (this.operationFrom == null) {
            if (this.operandText.equals("")) {
                this.operandText = "0";
            }
            this.calculatedValue = this.operandText;
            this.calculated = true;
            return true;
        } else {
            String value = this.operationFrom.calculate();
            this.setCalculationResult(value);
        }
        return this.calculated;
    }
    
    public void setCalculationResult(String value) {
        this.calculatedValue = value;
        this.calculated = true;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != this.getClass()) 
            return false;
        return this.compare((Operand)obj);
    }
    
    private boolean compare(Operand operand) {
        if (operand.isComplex != this.isComplex ||
            !operand.operandText.equals(this.operandText)) {
            return false;
        }
        return true;
    }
}
