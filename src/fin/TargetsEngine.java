/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

import java.util.ArrayList;

/**
 *
 * @author alexxx
 */
public class TargetsEngine {
    
    private Engine sqlEngine;
    
    public TargetsEngine(Engine engine) {
        this.sqlEngine = engine;
    }
    
    private Account GetAccount(ArrayList<Account> accounts, int accountId) {
        for(int i = 0; i < accounts.size(); i++) {
            Account acc = accounts.get(i);
            if (acc.id == accountId) {
                return acc;
            }
        }
        return null;
    }
    
    private ArrayList<Account> GetUsedAccounts(Target target, ArrayList<Account> accounts) {
        ArrayList<Account> result = new ArrayList<Account>();
        ArrayList<TargetAccount> usedAccounts = this.sqlEngine.getTargetAccounts(target);
        for (int i = 0; i < usedAccounts.size(); i++) {
            TargetAccount targetAccount = usedAccounts.get(i);
            result.add(GetAccount(accounts, targetAccount.accountId));
        }
        return result;
    }
    
    private double GetAccountValue(Account account, Target target) {
        if (account.currency.id == target.currency.id) {
            return account.amount;
        }
        return Convert(account.amount, account.currency, target.currency);
    }
    
    private void SetAccountValue(double accountValue, Account account, Target target) {
        if (account.currency.id == target.currency.id) {
            account.amount = accountValue;
            return;
        }
        account.amount = Convert(accountValue, target.currency, account.currency);
    }
    
    private double Convert(double ammount, Currency sourceCurrency,  Currency destCurrency) {
        return (ammount*sourceCurrency.rate)/destCurrency.rate;
    }
    
    private void SetTargetDoneAmmount(Target target, ArrayList<Account> accounts) {
        ArrayList<Account> usedAccounts = GetUsedAccounts(target, accounts);
        for (int i = 0; i < usedAccounts.size(); i++) {
            Account account = usedAccounts.get(i);
            if (account.amount == 0) {
                continue;
            }
            int need = target.ammount - target.doneAmmount;
            if (need <= 0) {
                return;
            }
            double accountValue = GetAccountValue(account, target);
            if (accountValue >= need) {
                accountValue -= need;
                target.doneAmmount += need;
            } else {
                target.doneAmmount += accountValue;
                accountValue = 0;
            }
            SetAccountValue(accountValue, account, target);
        }
    }
    
    public ArrayList<Target> getTargets() {
        ArrayList<Target> targets = this.sqlEngine.getTargets();
        ArrayList<Account> accounts = this.sqlEngine.getAccounts();
        for (int i = 0; i <targets.size(); i++) {
            Target target = targets.get(i);
            if (target.state == TargetState.Active) {
                SetTargetDoneAmmount(target, accounts);
            }
            if (target.state == TargetState.Done) {
                target.doneAmmount = target.ammount;
            }
        }
        return targets;
    }
    
    public ArrayList<TargetAccount> getTargetAccounts(Target target) {
        if (target == null) {
            return new ArrayList<TargetAccount>();
        }
        return this.sqlEngine.getTargetAccounts(target);
    }
    
    public void DeleteTargetAccount(TargetAccount targetAccount) {
        this.sqlEngine.DeleteTargetAccount(targetAccount);
        CustomEvent event = new CustomEvent("TargetAccountModified");
        event.throwEvent();
    }
    
    public void DeleteTarget(Target target) {
        this.sqlEngine.DeleteTarget(target);
        CustomEvent event = new CustomEvent("TargetAccountModified");
        event.throwEvent();
    }
    
    public void SaveTargetAccount(TargetAccount targetAccount) {
        this.sqlEngine.SaveNewTargetAccount(targetAccount);
        CustomEvent event = new CustomEvent("TargetAccountModified");
        event.throwEvent();
    }
    
    public void SaveTarget(Target target) {
        this.sqlEngine.SaveNewTarget(target);
        CustomEvent event = new CustomEvent("TargetAccountModified");
        event.throwEvent();
    }
}
