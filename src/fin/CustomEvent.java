/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

/**
 *
 * @author alexxx
 */
public class CustomEvent {
    private Observer observer;
    public String name = "";
    
    public CustomEvent(String name) {
        this.name = name;
        this.observer = EventUtils.getInstance().getObserver(this.name);
    }
    
    protected void throwEvent() {
        this.observer.processEvent();
    }
    
    protected void throwEvent(Object params) {
        this.observer.processEvent(params);
    }
}
