/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

import java.util.HashMap;

/**
 *
 * @author alexxx
 */
public class EventUtils {
    private static EventUtils instance; 
    private HashMap<String, Observer> observers;
    
    private EventUtils() {
        this.observers = new HashMap<String, Observer>();
    }
    
    protected static EventUtils getInstance() {
        if(instance == null) {
            instance = new EventUtils();
        }
        return instance;
    }
    
    protected Observer getObserver(String event) {
        Observer result = this.observers.get(event);
        if (result == null) {
            result = new Observer(event);
            this.observers.put(event, result);
        }
        return result;
    }
    
    protected void subscribeEvent(String event, ICustomEventListener listener) {
        Observer observer = this.getObserver(event);
        observer.addListener(listener);
    }
    
    protected void subscribeEvent(String event, ICustomEventWithParamsListener listener) {
        Observer observer = this.getObserver(event);
        observer.addListener(listener);
    }
}
