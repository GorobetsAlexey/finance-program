/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * CheckFrame.java
 *
 * Created on 01.01.2015, 12:29:52
 */
package fin;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.WebcamResolution;
import com.github.sarxos.webcam.WebcamUtils;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.table.TableColumn;

/**
 *
 * @author alexxx
 */
public class CheckFrame extends javax.swing.JFrame {

    /** Creates new form CheckFrame */
    public CheckFrame(Check check) {
        this.check = check;
        this.engine = Engine.getInstance();
        initComponents();
        addFileViewComponent();
        this.description.setText(check.description);
        String title = "Чек номер "+ check.guid+", за "+ check.date;
        if (check.amount < 0) {
            title +=  ", потрачено ";
        } else {
            title +=  ", получено ";
        }
        title += Math.abs(check.amount);
        setTitle(title);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        checkItems = new javax.swing.JTable();
        selectFileBtn = new javax.swing.JButton();
        takePhotoBtn = new javax.swing.JButton();
        drawFileScroll = new javax.swing.JScrollPane();
        jButton1 = new javax.swing.JButton();
        description = new javax.swing.JTextField();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        TableModelFactory factory = new TableModelFactory();
        checkItems.setModel(factory.getTableModel(this.engine.getItemsByCheck(this.check)));
        jScrollPane1.setViewportView(checkItems);
        TableColumn column = checkItems.getColumnModel().getColumn(0);
        column.setPreferredWidth(this.DATE_CELL_WIDTH);
        column.setMaxWidth(this.DATE_CELL_WIDTH);
        column.setResizable(false);
        column = checkItems.getColumnModel().getColumn(2);
        column.setPreferredWidth(this.AMMOUNT_CELL_WIDTH);
        column.setMaxWidth(this.AMMOUNT_CELL_WIDTH);
        column.setResizable(false);

        checkItems.setRowSelectionAllowed(false);
        checkItems.setColumnSelectionAllowed(false);
        checkItems.setRowSelectionAllowed(true);

        selectFileBtn.setText("Выбрать файл");
        selectFileBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectFileBtnActionPerformed(evt);
            }
        });

        takePhotoBtn.setText("Сделать снимок");
        takePhotoBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                takePhotoBtnActionPerformed(evt);
            }
        });

        jButton1.setText("Полный размер");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        description.setText("jTextField1");
        description.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                descriptionFocusLost(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(description, javax.swing.GroupLayout.DEFAULT_SIZE, 305, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(selectFileBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(takePhotoBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)
                        .addGap(0, 260, Short.MAX_VALUE))
                    .addComponent(drawFileScroll))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(description, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 555, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(drawFileScroll, javax.swing.GroupLayout.DEFAULT_SIZE, 547, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(selectFileBtn)
                            .addComponent(takePhotoBtn)
                            .addComponent(jButton1))))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void selectFileBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectFileBtnActionPerformed
        if (this.webcam != null) {
            this.webcam.close();
        }
        this.webBtnState = false;
        byte[] buffer = this.engine.readImage(this.check);
        this.engine.updateCheckList();
        CustomEvent event = new CustomEvent("CheckSaved");
        event.throwEvent();
        this.view = new CheckFileView(buffer);
        drawFileScroll.setViewportView(view);
        drawFileScroll.updateUI();
    }//GEN-LAST:event_selectFileBtnActionPerformed

    private void takePhotoBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_takePhotoBtnActionPerformed
        if (!this.webBtnState) {
            if (this.webcam != null) {
                this.webcam.close();
            }
            this.webcam = Webcam.getDefault();
            this.webcam.setViewSize(WebcamResolution.VGA.getSize());
            WebcamPanel panel = new WebcamPanel(this.webcam);
            panel.setFPSDisplayed(true);
            panel.setDisplayDebugInfo(true);
            panel.setImageSizeDisplayed(true);
            panel.setMirrored(true);
            this.webBtnState = true;
            drawFileScroll.setViewportView(panel);
            drawFileScroll.updateUI();
        } else {
            byte[] bytes = WebcamUtils.getImageBytes(webcam, "png");
            this.webcam.close();
            engine.updateCheckFile(check, bytes);
            engine.updateCheckList();
            CustomEvent event = new CustomEvent("CheckSaved");
            event.throwEvent();
            this.view = new CheckFileView(bytes);
            this.webBtnState = false;
            drawFileScroll.setViewportView(this.view);
            drawFileScroll.updateUI();
        }
    }//GEN-LAST:event_takePhotoBtnActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (this.view != null) {
            this.view.toggleScale();
            this.jScrollPane1.updateUI();
            String title = this.view.scale ? "Полный размер" : "По размеру окна";
            this.jButton1.setText(title);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void descriptionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_descriptionFocusLost
        String newDescription = this.description.getText();
        if (newDescription.equals(this.check.description)) {
            return;
        }
        this.check.description = newDescription;
        this.engine.UpdateCheck(check);
        this.engine.updateCheckList();
        CustomEvent event = new CustomEvent("CheckSaved");
        event.throwEvent();
    }//GEN-LAST:event_descriptionFocusLost

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JTable checkItems;
    private javax.swing.JTextField description;
    private javax.swing.JScrollPane drawFileScroll;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton selectFileBtn;
    private javax.swing.JButton takePhotoBtn;
    // End of variables declaration//GEN-END:variables
    private Check check;
    private Engine engine;
    private Webcam webcam;
    private CheckFileView view;
    private boolean webBtnState = false;
    private final int DATE_CELL_WIDTH = 70;
    private final int AMMOUNT_CELL_WIDTH = 60;
    
    private void addFileViewComponent() {
        if (check.file == null || check.file.length == 0) {
            return;
        }
        this.view = new CheckFileView(check.file);
        drawFileScroll.setViewportView(view);
    }
}