/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author alexxx
 */
public class TargetAccount {
    
    public int id;
    
    public int targetId;
    
    public int accountId;
    
    public String accountName;
    
    public String accountCurrencyName;
    
    public int usedInAccounts;
    
    public TargetAccount() {}
    
    public TargetAccount(ResultSet rs) throws SQLException {
        this.id = rs.getInt("Id");
        this.targetId = rs.getInt("Target");
        this.accountId = rs.getInt("Account");
        this.accountName = rs.getString("Description");
        this.accountCurrencyName = rs.getString("Name");
        this.usedInAccounts = rs.getInt("usages");
    }
    
    public static String getAllSelect() {
        return "select \n" +
        "	ta.\"Id\",\n" +
        "	ta.\"Account\",\n" +
        "	ta.\"Target\",\n" +
        "	a.\"Description\",\n" +
        "	c.\"Name\",\n" +
        "	(select count(1) from \"TargetAccount\" ta1\n" +
        "		inner join \"Target\" t on t.\"Id\" = ta1.\"Target\"\n" +
        "		where \"Account\" = ta.\"Account\" and \n" +
        "		t.\"status\" = 0) as usages\n" +
        "from \"TargetAccount\" ta\n" +
        "inner join \"Account\" a  on ta.\"Account\" = a.\"Id\"\n" +
        "inner join \"Currency\" c  on a.\"Currency\" = c.\"CurrencyId\"\n";
    }
    
    public static String getSelect(Target target) {
        return getAllSelect() + "where ta.\"Target\" = " + target.id + "\n" +
                "order by usages;";
    }
    
    public String getDelete() {
        return "DELETE FROM \"TargetAccount\" WHERE \"Id\" = "+ this.id;
    }
    
    public String getInsert() {
        return "INSERT INTO \"TargetAccount\" (\"Target\", \"Account\") values (" + this.targetId + "," + this.accountId + ");";
    }
}
