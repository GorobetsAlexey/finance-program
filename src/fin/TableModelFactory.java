/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author alexxx
 */
class TableModelFactory {
    public AbstractTableModel getCheckTableModel() {
       return new CheckTableModel();
    }
    
    public DataRowTableModel getTableModel(ArrayList<DataRow> data) {
        return new DataRowTableModel(data);
    }
    
     public AccountDiffTableModel getAccountDiffTableModel(ArrayList<AccountDiff> data) {
        return new AccountDiffTableModel(data);
    }
    
    public AccountTableModel getAccountTableModel(ArrayList<Account> data) {
        return new AccountTableModel(data);
    }
    
    public CurrencyTableModel getCurrencyTableModel(ArrayList<Currency> data) {
        return new CurrencyTableModel(data);
    }
    
    public TargetsTableModel getTargetsTableModel(ArrayList<Target> data) {
        return new TargetsTableModel(data);
    }
    public TargetAccountsTableModel getTargetAccountsTableModel(ArrayList<TargetAccount> data) {
        return new TargetAccountsTableModel(data);
    }
}

class CheckTableModel extends AbstractTableModel {
    private Engine engine;
    
    public CheckTableModel() {
        this.engine = Engine.getInstance();
    }
    
    @Override
    public String getColumnName(int col) {
        switch(col){
            case 0:
                return "Дата";
            case 1:
                return "Описание";
            case 2:
                return "Сумма";
            case 3:
                return "Фото";
        }
        return "";
    }

    @Override
    public int getRowCount() { 
        return engine.getCheckCount(); 
    }
    
    @Override
    public int getColumnCount() { 
        return 4; 
    }
    
    @Override
    public Object getValueAt(int row, int col) {
        Check check = engine.getCheck(row);
        switch(col){
            case 0:
                return check.date;
            case 1:
                return check.description;
            case 2:
                return check.amount;
            case 3:
                if (check.file == null) return "-";
                if (check.file.length == 0) return "?";
                return "+";
        }
        return "";
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
        if (col == 1) {
            Check check = engine.getCheck(row);
            return check.description == null || check.description.isEmpty();
        }
        return false;
    }
    
    @Override
    public void setValueAt(Object value, int row, int col) {
        if (col == 1) {
            Check check = engine.getCheck(row);
            check.description = (String)value;
            engine.UpdateCheck(check);
        }
    }
}

class DataRowTableModel extends AbstractTableModel {
    private ArrayList<DataRow> data;
    
    public DataRowTableModel(ArrayList<DataRow> data) {
        this.data = data;
    }

    @Override
    public String getColumnName(int col) {
        switch(col){
            case 0:
                return "Дата";
            case 1:
                return "Теги";
            case 2:
                return "Сумма";
        }
        return "";
    }

    @Override
    public int getRowCount() { 
        return this.data.size(); 
    }
    
    @Override
    public int getColumnCount() { 
        return 3; 
    }
    
    @Override
    public Object getValueAt(int row, int col) {
        DataRow dataRow = this.data.get(row);
        switch(col){
            case 0:
                return dataRow.date;
            case 1:
                return dataRow.tag;
            case 2:
                return dataRow.getAmount();
        }
        return "";
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
       return false;
    }
    
    @Override
    public void setValueAt(Object value, int row, int col) {}
}

class AccountTableModel extends AbstractTableModel {

    private ArrayList<Account> data;
    
    public AccountTableModel(ArrayList<Account> data) {
        this.data = data;
    }
   
    public Account getRow(int rowIndex) {
        return this.data.get(rowIndex);
    }
    
    @Override
    public String getColumnName(int col) {
        switch(col){
            case 0:
                return "Описание";
            case 1:
                return "";
            case 2:
                return "Тип";
            case 3:
                return "Остаток";
        }
        return "";
    }

    @Override
    public int getRowCount() {
        return this.data.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Account row = this.data.get(rowIndex);
        switch(columnIndex) {
            case 0: 
                return row.description;
            case 1:
                return row.currency;
            case 2:
                return row.type;
            case 3:
                return row.amount;
        }
        return null;
    }
    
}

class TargetAccountsTableModel extends AbstractTableModel {
     private ArrayList<TargetAccount> data;
     private MathEngine mathEngine;
     
     public TargetAccountsTableModel(ArrayList<TargetAccount> data) {
        this.data = data;
        this.mathEngine = new MathEngine();
    }
     
    public void setData(ArrayList<TargetAccount> data) {
        this.data = data;
    }
    
    public void addRowTop(TargetAccount row) {
        this.data.add(0, row);
    }

    public TargetAccount getRow(int rowIndex) {
        return this.data.get(rowIndex);
    }
    
    @Override
    public String getColumnName(int col) {
        switch(col){
            case 0:
                return "Счет";
            case 1:
                return "Валюта";
        }
        return "";
    }

    @Override
    public int getRowCount() { 
        return this.data.size(); 
    }
    
    @Override
    public int getColumnCount() { 
        return 2; 
    }
    
    @Override
    public Object getValueAt(int row, int col) {
        TargetAccount dataRow = this.data.get(row);
        switch(col){
            case 0:
                return dataRow.accountName;
            case 1:
                return dataRow.accountCurrencyName;
        }
        return "";
    }
    
    public Class getColumnClass(int c) {
        if (this.data != null && !this.data.isEmpty()) {
          return getValueAt(0, c).getClass();
        }
        return "".getClass();
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
      if (col > 0) {
          return false;
      }
      return this.data != null && !this.data.isEmpty() && this.data.get(row).id == -1;
    }
    
    @Override
    public void setValueAt(Object value, int row, int col) {     
        TargetAccount rowObject = this.data.get(row);
        if (col == 0) {
            Account acc = (Account)value;
            rowObject.accountId = acc.id;
            rowObject.accountName = acc.description;
            setValueAt(acc.currency.name, row, 1);
        } else {
            rowObject.accountCurrencyName = (String)value;
        }
    }
}

class TargetsTableModel extends AbstractTableModel {
    private ArrayList<Target> data;
    private MathEngine mathEngine;
    
    public TargetsTableModel(ArrayList<Target> data) {
        this.data = data;
        this.mathEngine = new MathEngine();
    }
    
    public void setData(ArrayList<Target> data) {
        this.data = data;
    }
    
    public void addRowTop(Target row) {
        this.data.add(0, row);
    }

    public Target getRow(int rowIndex) {
        return this.data.get(rowIndex);
    }
    
    public Class getColumnClass(int c) {
        if (this.data != null && !this.data.isEmpty()) {
          return getValueAt(0, c).getClass();
        }
        return "".getClass();
    }

    @Override
    public String getColumnName(int col) {
        switch(col){
            case 0:
                return "Приоритет";
            case 1:
                return "Название";
            case 2:
                return "Сумма";
            case 3:
                return "Валюта";
        }
        return "";
    }

    @Override
    public int getRowCount() { 
        return this.data.size(); 
    }
    
    @Override
    public int getColumnCount() { 
        return 4; 
    }
    
    @Override
    public Object getValueAt(int row, int col) {
        Target dataRow = this.data.get(row);
        switch(col){
            case 0:
                return dataRow.priority;
            case 1:
                return dataRow.description;
            case 2:
                return dataRow.ammount;
            case 3:
                return dataRow.currency != null ? dataRow.currency.name : "";
        }
        return "";
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
      Target target = getRow(row);
      return target.id == -1;
    }
    
    @Override
    public void setValueAt(Object value, int row, int col) {  
        Target rowObject = this.data.get(row);
        switch(col) {
            case 0:
                rowObject.priority = (int)value;
                break;
            case 1:
                rowObject.description = (String)value;
                break;
            case 2:
                rowObject.ammount = (int)value;
                break;
            case 3:
                rowObject.currency = (Currency)value;
                break;
        }
    }
}

class AccountDiffTableModel extends AbstractTableModel {
    private ArrayList<AccountDiff> data;
    private MathEngine mathEngine;
    
    public AccountDiffTableModel(ArrayList<AccountDiff> data) {
        this.data = data;
        this.mathEngine = new MathEngine();
    }
    
    public void setData(ArrayList<AccountDiff> data) {
        this.data = data;
    }
    
    public void addRowTop(AccountDiff row) {
        this.data.add(0, row);
    }

    public AccountDiff getRow(int rowIndex) {
        return this.data.get(rowIndex);
    }

    @Override
    public String getColumnName(int col) {
        switch(col){
            case 0:
                return "Дата";
            case 1:
                return "Было";
            case 2:
                return "Стало";
            case 3:
                return "Разница";
        }
        return "";
    }

    @Override
    public int getRowCount() { 
        return this.data.size(); 
    }
    
    @Override
    public int getColumnCount() { 
        return 4; 
    }
    
    @Override
    public Object getValueAt(int row, int col) {
        AccountDiff dataRow = this.data.get(row);
        switch(col){
            case 0:
                return dataRow.date;
            case 1:
                return dataRow.before;
            case 2:
                return dataRow.after;
            case 3:
                return dataRow.diff;
        }
        return "";
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
       AccountDiff rowEl = this.data.get(row);
       return (col != 1) && rowEl.id == -1;
    }
    
    @Override
    public void setValueAt(Object value, int row, int col) {
        AccountDiff rowEl = this.data.get(row);
        Engine engine = Engine.getInstance();
        String rawValue = ((String)value).replace(",", ".");
        switch(col) {
            case 0:
                rowEl.date = rawValue;
                break;
            case 2:                
                if (this.mathEngine.isExpression(rawValue)){
                    try {
                       rawValue = this.mathEngine.calculateExpression(rawValue);
                    } catch (ExpressionFormatException ex) {
                    }
                }
                rowEl.after = engine.round(Double.parseDouble(rawValue), 2);
                rowEl.diff = engine.round(rowEl.after - rowEl.before, 2);
                break;
            case 3:
                if (this.mathEngine.isExpression(rawValue)){
                    try {
                       rawValue = this.mathEngine.calculateExpression(rawValue);
                    } catch (ExpressionFormatException ex) {
                    }
                }
                rowEl.diff = engine.round(Double.parseDouble(rawValue), 2);
                rowEl.after = engine.round(rowEl.before + rowEl.diff, 2);
                break;
        }
    }
}

class CurrencyTableModel extends AbstractTableModel {
    private ArrayList<Currency> data;
    
    public CurrencyTableModel(ArrayList<Currency> data) {
        this.data = data;
    }
    @Override
    public int getRowCount() {
        return this.data.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Currency row = this.data.get(rowIndex);
        switch (columnIndex) {
            case 0: 
                return row.name;
            case 1:
                return row.rate;
        }
        return "";
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
        return true;
    }
    
    @Override
    public void setValueAt(Object value, int row, int col) {
        Currency rowEl = this.data.get(row);
        Engine engine = Engine.getInstance();
        switch(col) {
            case 0:
                rowEl.name = (String)value;
                break;
            case 1:
                rowEl.rate = engine.round(Double.parseDouble((String)value), 2);
                break;
        }
        rowEl.save();
    }
    
     @Override
    public String getColumnName(int col) {
        switch(col){
            case 0:
                return "Валюта";
            case 1:
                return "Курс";
        }
        return "";
    }
}