/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

import java.util.EmptyStackException;
import java.util.Stack;

/**
 *
 * @author alexxx
 */
public class Automat {
   public char[] expresion;
   public Stack<Integer> stack;
   public int state;
   public int currChar;
   public AutomatTable algoritm;
   public boolean finished;
   public boolean valid;
   
   public static final int SIGN = 0;
   public static final int MINUS = 1;
   public static final int NUMBER = 2;
   public static final int LETER = 3;
   public static final int OPENING_BRACKET = 4;
   public static final int CLOSING_BRACKET = 5;
   public static final int POINT = 6;
   
   public static final int SIGN_ERR = -1;
   public static final int NUMBER_POS_ERR = -2;
   public static final int NUMBER_FORMAT_ERR = -3;
   public static final int BRACKET_ERR = -4;
   public static final int FLOAT_NUMBER_FORMAT_ERR = -5;
   public static final int VARIABLE_NAME_FORMAT_ERR = -6;
   
   public Automat (AutomatTable algoritm, String expresion) {
       this.stack = new Stack<Integer> ();
       this.state = 0;
       this.algoritm = algoritm;
       this.finished = false;
       this.currChar = 0;
       this.expresion = (clearFromSpaces(expresion)).toCharArray();
   }
   
   public final String clearFromSpaces (String in) {
       String result = "";
       for (int i = 0; i < in.length(); i++)
           if (in.charAt(i) != ' ')
               result += in.charAt(i);
       return result;
   }
   
   public int inThisInterval(char symbol) {
       if ((byte)symbol == 40)
           return OPENING_BRACKET;
       if ((byte)symbol == 41)
           return CLOSING_BRACKET;
       if ((byte)symbol == 42 || (byte)symbol == 43 || (byte)symbol == 47)
           return SIGN;
       if ((byte)symbol == 45)
           return MINUS;
       if ((byte)symbol >= 48 && (byte)symbol <= 57)
           return NUMBER;
       if ((byte)symbol == 46)
           return POINT;
       if ((byte)symbol >= 65 && (byte)symbol <= 122)
           return LETER;       
       return -1;
   }  
   
   public void showMesg (int pos, String mesg) {
       System.out.println();
       for (int i = 0; i <= Math.min(this.expresion.length-1, pos); i++)
           System.out.print (this.expresion[i]);
      System.out.println();
      for (int i = 0; i <= Math.min(this.expresion.length-1, pos-1); i++)
           System.out.print (" ");      
      System.out.print ("^");
      System.out.printf("\n Reached %d char. %s\n", pos, mesg);
   }
   
   public void doStep () {
       if (this.currChar < this.expresion.length) {
           if (!this.finished) {                
                int column = inThisInterval(this.expresion[this.currChar]);
                int nextState = this.algoritm.getNextState(this.state, column);
                if (nextState >= 0) {
                    this.state = nextState;
                    if (this.state == 3) {//todo const
                        this.stack.push(currChar);
                    }
                    if (this.state == 4) {//todo const
                        try {
                            this.stack.pop();
                        } catch (EmptyStackException e){
                            showMesg(currChar, createErrMessage(BRACKET_ERR));
                            this.finished = true;
                            this.valid = false;
                        }
                    }
                } else {
                    showMesg(currChar, createErrMessage(nextState));
                    this.finished = true;
                    this.valid = false;
                }                
                this.currChar++;                
           }
       } else { 
           if (this.stack.empty() && inThisInterval(this.expresion[this.currChar-1]) != SIGN && inThisInterval(this.expresion[this.currChar-1]) != MINUS) {
               this.valid = true;        
               showMesg(this.expresion.length, "No errors found.");
           } else {
               if (!this.stack.empty()) {
                   showMesg(currChar, createErrMessage(BRACKET_ERR));
               } else {
                    showMesg(currChar, createErrMessage(SIGN_ERR));
               }
               this.valid = false; 
            }
            this.finished = true;
       }
   }
   
   public String  createErrMessage(int err) {
       if (err == SIGN_ERR)
           return "Wrong sign use";
       if (err == NUMBER_POS_ERR)
           return "Some number error";
       if (err == NUMBER_FORMAT_ERR)
           return "Number format error";
       if (err == BRACKET_ERR)
           return "Bracket use error";
       if (err == FLOAT_NUMBER_FORMAT_ERR)
           return "Float number format error";
       if (err == VARIABLE_NAME_FORMAT_ERR)
           return "Varible name format error";
        return "";
   }
}
