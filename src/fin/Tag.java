/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

import java.io.Serializable;

/**
 *
 * @author alexxx
 */
public class Tag implements Comparable<Tag>, Serializable{
    public String tag;
    public int useCount = 1;

    public Tag(String tag){
        this.tag = tag;
    }
    
    @Override
    public int compareTo(Tag o) {
       if (this.tag.equals(o.tag))
           return 0;
       else return o.useCount - this.useCount ;
    }
    
    @Override
    public boolean equals(Object o) {
        if(!(o instanceof Tag)) return false;
        return this.tag.equals(((Tag)o).tag);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + (this.tag != null ? this.tag.hashCode() : 0);
        return hash;
    }
    
    @Override
    public String toString(){
        return this.tag;
    }
}
