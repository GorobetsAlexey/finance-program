/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

import java.util.ArrayList;
import java.util.Stack;

/**
 *
 * @author alexxx
 */
public class MathEngine {
    public int currId = 0;
    public String expr;
    
    public int getId () {
        this.currId++;
        return this.currId;
    }
    
    public MathEngine () {
    }
    
    public MathEngine (String expression) {
        this.expr = expression;
        Automat check = new Automat(new AutomatTable(), this.expr);
        this.expr = new String(check.expresion);
        while (needToChange(this.expr))
            this.expr = changeExpression(this.expr);
    }
    
    public String addOperand (Operand operand) {
        String result = "";
        if (operand.isComplex)
            result += "(";
        result += operand.operandText;
        if (operand.isComplex)
            result += ")";
        return result;
    }
    
    public String createString (ArrayList<ExpresionElement> expr) {
        String result = "";
        for (int i = 0; i < expr.size(); i++) {
            if (expr.get(i).getType() == 0)
                result += addOperand((Operand)expr.get(i));
            if (expr.get(i).getType() == 1)
                result += ((Operation)expr.get(i)).operationText;
        }            
        return result;
    }
    
    public final boolean needToChange(String expressionStr){
            ArrayList<ExpresionElement> expElems = createInTypeExpresion(expressionStr);
            for (int i = 1; i < expElems.size()-3; i+=2){
                Operation op1 = (Operation)expElems.get(i);
                if (op1.operationText.equals("-") || op1.operationText.equals("/")){
                    Operation op2 = (Operation)expElems.get(i+2);
                    if (op2.operationText.equals(op1.operationText))
                        return true;
                }
            }
            return false;
    }
    
    public final String changeExpression (String expressionStr){
        ArrayList<ExpresionElement> expElems = createInTypeExpresion(expressionStr);
        ArrayList<ExpresionElement> expRes = new ArrayList<ExpresionElement>();
        expRes.add(expElems.get(0));
        boolean flag = false;
        
        for (int i = 0; i < expElems.size(); i++)
            if (expElems.get(i).getType() == 0){
                if (((Operand)expElems.get(i)).isComplex)
                    ((Operand)expElems.get(i)).operandText = changeExpression(((Operand)expElems.get(i)).operandText);
            }
        
        
        for (int i = 1; i < expElems.size()-3; i += 2) {
            if (expElems.get(i).getType()== 1){
                Operation op1 = (Operation)expElems.get(i);
                Operation op2 = (Operation)expElems.get(i+2);
                if (op1.operationText.equals("+") ||
                    op1.operationText.equals("*")) {
                    expRes.add(op1);
                    expRes.add(expElems.get(i+1));                    
                }
                if (op1.operationText.equals("-")) {
                    if (op1.operationText.equals(op2.operationText)) {
                        if (i+4 < expElems.size()-1) {
                            if (!((Operation)expElems.get(i+4)).operationText.equals("/") &&
                                !((Operation)expElems.get(i+4)).operationText.equals("*")){
                                expRes.add(op1);
                                Operand buf = new Operand();
                                buf.id = getId();
                                buf.isComplex = true;
                                buf.operandText = addOperand((Operand)expElems.get(i+1))+"+"+addOperand((Operand)expElems.get(i+3));
                                expRes.add(buf);
                                i += 2;                                
                            } else {
                                expRes.add(op1);
                                expRes.add(expElems.get(i+1));
                            }
                        } else {
                            expRes.add(op1);
                            Operand buf = new Operand();
                            buf.id = getId();
                            buf.isComplex = true;
                            buf.operandText = addOperand((Operand)expElems.get(i+1))+"+"+addOperand((Operand)expElems.get(i+3));
                            expRes.add(buf);
                            i += 2;
                            flag = true;
                        }
                    } else {
                        expRes.add(op1);
                        expRes.add(expElems.get(i+1));
                    }
                }
                if (op1.operationText.equals("/")) {
                    if (op1.operationText.equals(op2.operationText)) {
                        expRes.add(op1);
                        Operand buf = new Operand();
                        buf.id = getId();
                        buf.isComplex = true;
                        buf.operandText = addOperand((Operand)expElems.get(i+1))+"*"+addOperand((Operand)expElems.get(i+3));
                        expRes.add(buf);
                        i += 2;
                        if (i+2 >= expElems.size()-1)
                            flag = true;
                    } else {
                        expRes.add(op1);
                        expRes.add(expElems.get(i+1));
                    }
                }
            }
        }
        
        if (!flag) {
            expRes.add(expElems.get(expElems.size()-2));
            expRes.add(expElems.get(expElems.size()-1));
        }
        return createString(expRes);
    }
    
    public ArrayList<ExpresionElement> createInTypeExpresion (String expresion) {
        Automat check = new Automat(new AutomatTable(), expresion);
        while (!check.finished)
            check.doStep();
        if (check.valid) {
            expresion = expresion.replaceAll("-\\(", "-1*(");
            ArrayList<ExpresionElement> result = new  ArrayList<ExpresionElement>();
            for (int i = 0; i < expresion.length(); i++){
                Operand tmpOperand = new Operand();
                tmpOperand.id = getId();            
                Operation tmpOperation = new Operation();
                tmpOperation.id = getId();
                if ((byte)expresion.charAt(i) != 40) {                
                    tmpOperand.operandText = expresion.substring(i, getNextSignIndex(i, expresion));
                    tmpOperand.isComplex = false;
                    i = getNextSignIndex(i, expresion);
                } else {
                    tmpOperand.operandText = expresion.substring(i+1, getCloseBracetIndex(i, expresion));
                    tmpOperand.isComplex = true;
                    i = getCloseBracetIndex(i, expresion);
                    i++;
                }
                result.add(tmpOperand);            
                if (i < expresion.length()) {
                    tmpOperation.operationText = expresion.charAt(i)+"";            
                    result.add(tmpOperation);
                    if(tmpOperation.operationText.equals("-")){
                        tmpOperation.operationText = "+";
                        i--;
                    }
                }
            }

            return result;
        }
        return null;
    }
    
    public int getNextSignIndex (int startIndex, String expresion) {
        for (int i = startIndex+1; i < expresion.length(); i++)
            if ((byte)expresion.charAt(i) == 42 ||
                (byte)expresion.charAt(i) == 43 || 
                (byte)expresion.charAt(i) == 47 ||
                (byte)expresion.charAt(i) == 45)        
                    return i;
        return expresion.length();
    }
    
    public int getCloseBracetIndex (int openBracketIndex, String expresion) {
        Stack<Integer> stack = new Stack<Integer>();
        for (int i = openBracketIndex; i < expresion.length(); i++){
            if ((byte)expresion.charAt(i) == 40)
                   stack.push(i);
            if ((byte)expresion.charAt(i) == 41)
                   if (stack.size() == 1) {
                       return i;
                   }
                   else
                       stack.pop();
        }
        return -1;
    }
    
    public boolean checkExpresion (String expresion) {
        AutomatTable table = new AutomatTable();
        Automat automat = new Automat(table, expresion);
        while (!automat.finished)
            automat.doStep();
        return automat.valid;
    }
    
    public boolean isOperandUsed (ArrayList<Operation> currLevel, Operand operand) {
        for (int i = 0 ; i < currLevel.size(); i++) 
            if (currLevel.get(i).inOperand1.id == operand.id || 
                currLevel.get(i).inOperand2.id == operand.id)
                return true;
        return false;
    }
    
    public boolean isOperationUsed (ArrayList<Operation> currLevel, Operation operation) {
        for (int i = 0 ; i < currLevel.size(); i++)
            if (currLevel.get(i).id == operation.id)
                return true;
        return false;
    }
    
    public Operand getLastResultWhithThisOperand (Operand operand) {
        Operand result = operand;
        while (result.operationTo != null)
            result = result.operationTo.outOperand;
        return result;
    }
    
    public boolean allMullDiv(ArrayList<ExpresionElement> currInTypeExpression, ArrayList<Operation> currLevel) {
        for (int i = 0; i < currInTypeExpression.size(); i++) 
            if (currInTypeExpression.get(i).getType() == 1)
                if (((Operation)currInTypeExpression.get(i)).operationText.equals("*") ||
                    ((Operation)currInTypeExpression.get(i)).operationText.equals("/"))
                    if (!isOperationUsed(currLevel, (Operation)currInTypeExpression.get(i)))
                        return false;
        return true;
    }
    
    public boolean allUsed(ArrayList<ExpresionElement> currInTypeExpression, ArrayList<Operation> currLevel) {
        for (int i = 0; i < currInTypeExpression.size(); i++) 
            if (currInTypeExpression.get(i).getType() == 1)
                if (!isOperationUsed(currLevel, (Operation)currInTypeExpression.get(i)))
                    return false;
        return true;
    }
    
    public ArrayList<Operation> createOperations (ArrayList<ExpresionElement> expresion) {
        ArrayList<Operation> result = new ArrayList<Operation>();
         for (int i = 0; i < expresion.size(); i++){
            if (expresion.get(i).getType() == 1) 
                if (!isOperationUsed(result, (Operation)expresion.get(i)) &&
                    (((Operation)expresion.get(i)).operationText.equals("*") ||
                     ((Operation)expresion.get(i)).operationText.equals("/"))) {
                     if (!isOperandUsed(result, (Operand)expresion.get(i-1)) &&
                         !isOperandUsed(result, (Operand)expresion.get(i+1))) {
                         ((Operation)expresion.get(i)).inOperand1 = (Operand)expresion.get(i-1);
                         ((Operation)expresion.get(i)).inOperand2 = (Operand)expresion.get(i+1); 
                         ((Operation)expresion.get(i)).inOperand1.operationTo = (Operation)expresion.get(i);  
                         ((Operation)expresion.get(i)).inOperand2.operationTo = (Operation)expresion.get(i);  
                         ((Operation)expresion.get(i)).outOperand = new Operand();
                         ((Operation)expresion.get(i)).outOperand.operationFrom = (Operation)expresion.get(i);
                         ((Operation)expresion.get(i)).outOperand.isComplex = false;
                         ((Operation)expresion.get(i)).outOperand.id = getId();
                         ((Operation)expresion.get(i)).outOperand.operandText = ((Operation)expresion.get(i)).inOperand1.operandText + 
                                                                                           ((Operation)expresion.get(i)).operationText + 
                                                                                           ((Operation)expresion.get(i)).inOperand2.operandText;
                         result.add((Operation)expresion.get(i));
                     }
                     
                }
        }
         
         for (int i = 0; i < expresion.size(); i++){
            if (expresion.get(i).getType() == 1) 
                if (!isOperationUsed(result, (Operation)expresion.get(i)) ) {
                     if (!isOperandUsed(result, (Operand)expresion.get(i-1)) &&
                         !isOperandUsed(result, (Operand)expresion.get(i+1))) {
                         Operation operation = ((Operation)expresion.get(i));
                         operation.inOperand1 = (Operand)expresion.get(i-1);
                         operation.inOperand2 = (Operand)expresion.get(i+1); 
                         operation.inOperand1.operationTo = (Operation)expresion.get(i);  
                         operation.inOperand2.operationTo = (Operation)expresion.get(i);  
                         operation.outOperand = new Operand();
                         operation.outOperand.operationFrom = (Operation)expresion.get(i);
                         operation.outOperand.isComplex = false;
                         operation.outOperand.id = getId();
                         operation.outOperand.operandText = operation.inOperand1.operandText + 
                                                                                        operation.operationText + 
                                                                                        operation.inOperand2.operandText;
                         result.add(operation);
                     }
                     
                }
        }
         
        return result;
    }
    
    public Operand createExpresionTree (String expresion) {
        ArrayList<ExpresionElement> currInTypeExpression = createInTypeExpresion(expresion); 
        if (currInTypeExpression != null) {
        ArrayList<Operation> currLevel = new ArrayList<Operation>();
        ArrayList<Operation> buff = createOperations(currInTypeExpression);
        for (int i = 0; i < buff.size(); i++)
            currLevel.add(buff.get(i));
        while (!allUsed(currInTypeExpression, currLevel)){
            ArrayList<ExpresionElement> curExpresion = new ArrayList<ExpresionElement>();
            for (int i = 0; i < currInTypeExpression.size(); i++) {
                if (currInTypeExpression.get(i).getType() == 0)
                    if (!isOperandUsed(currLevel, (Operand)currInTypeExpression.get(i)))
                        curExpresion.add(currInTypeExpression.get(i));
                    else
                        if (curExpresion.lastIndexOf(getLastResultWhithThisOperand((Operand)currInTypeExpression.get(i))) == -1)
                            curExpresion.add(getLastResultWhithThisOperand((Operand)currInTypeExpression.get(i)));
                if (currInTypeExpression.get(i).getType() == 1)
                   if (!isOperationUsed(currLevel, (Operation)currInTypeExpression.get(i)))
                       curExpresion.add(currInTypeExpression.get(i));                    
            }
            buff = createOperations(curExpresion);
            for (int i = 0; i < buff.size(); i++)
            currLevel.add(buff.get(i));
        }
        for (int i = 0; i < currInTypeExpression.size(); i++)
            if (currInTypeExpression.get(i).getType() == 0 )
                if (((Operand)currInTypeExpression.get(i)).isComplex){
                    Operand bracketTop = createExpresionTree(((Operand)currInTypeExpression.get(i)).operandText);
                    bracketTop.operationTo = ((Operand)currInTypeExpression.get(i)).operationTo;
                    if (((Operand)currInTypeExpression.get(i)).operationTo.inOperand1.id == ((Operand)currInTypeExpression.get(i)).id) 
                        ((Operand)currInTypeExpression.get(i)).operationTo.inOperand1 = bracketTop;
                    else
                        ((Operand)currInTypeExpression.get(i)).operationTo.inOperand2 = bracketTop;
                    ((Operand)currInTypeExpression.get(i)).operationTo = null;
                }
        return getLastResultWhithThisOperand(currLevel.get(0).outOperand);        
        }
        return null;
    }
    
    public void printTree(Operand root) {
       ArrayList<Operation> list = new ArrayList<Operation>();
       list.add(root.operationFrom);
       while (list.size() > 0) {
           ArrayList<Operation> buf = new ArrayList<Operation>();
           for (int i = 0; i < list.size(); i++){
               Operation curr = list.get(i);
               System.out.printf ("%s -> %s <- %s\t", curr.inOperand1.operandText, curr.operationText, curr.inOperand2.operandText);
               if (curr.inOperand1.operationFrom != null)
                   buf.add (curr.inOperand1.operationFrom);
               if (curr.inOperand2.operationFrom != null)
                   buf.add (curr.inOperand2.operationFrom);
           }
           list = buf;
           System.out.println();
       }
   }
    
    public boolean isExpression(String line) {
        int index = getNextSignIndex(0, line);
        if (index != line.length()) {
            Operand result = createExpresionTree(line);
            return (result != null);
        }
         return false;
    }
    
    public String calculateExpression(String expression) throws ExpressionFormatException {
        Operand result = createExpresionTree(expression);
        if (result.calculated() && !result.calculatedValue.isEmpty()) {
            return result.calculatedValue;
        } 
        throw new ExpressionFormatException("Invalid expression");
    }
}
