/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fin;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import javax.swing.JPanel;

/**
 *
 * @author alexxx
 */
public class GraphPanel extends JPanel {
    
    private int xScale = 20;
    private double yScale = 1.0;
    private int width;
    private int heigth;
    private int zeroPosition;
    private int xMargin = 100;
    private int yMargin = 100;
    private ArrayList<DataRow> points;
    private ArrayList<DrawableDataRow> drawPoints;
    private DataRow maxY;
    private DataRow minY;
    private DrawableDataRow selectedPoint;
    
    public GraphPanel(ArrayList<DataRow> points) {
        SetPoints(points, false);
    }
    
    public void SetPoints(ArrayList<DataRow> points, boolean updateUI) {
        this.points = points;
        this.drawPoints = prepareDataRows(points);
        if (updateUI) {
            this.updateUI();
        }        
    }

    @Override
    public void paint(Graphics g) {        
        super.paint(g);
        this.regenerate();        
        this.setBackground(Color.WHITE);
        this.paintAxes(g);
        this.paintPoints(g);
        this.paintLegend(g);
    }
    
    private void paintAxes(Graphics g) {
        g.setColor(Color.GRAY);
        g.drawLine(this.xMargin, this.yMargin, this.xMargin, this.heigth + this.yMargin);
        g.drawLine(this.xMargin, this.zeroPosition, this.xMargin+this.width, this.zeroPosition);
        g.setColor(Color.BLACK);
    }
    
    private void paintPoints(Graphics g) {
        for (int i = 0; i < this.drawPoints.size() - 1; i++) {
            DrawableDataRow firstPoint = this.drawPoints.get(i);
            DrawableDataRow secondPoint = this.drawPoints.get(i + 1);
            setPointPosition(firstPoint);
            setPointPosition(secondPoint);
            setLineColor(firstPoint, secondPoint, g);
            drawThickLine(firstPoint.x, firstPoint.y, secondPoint.x, secondPoint.y, g);
            firstPoint.draw(g);
            secondPoint.draw(g);
        }
    }
    
    private void drawThickLine(int x1, int y1, int x2, int y2, Graphics g) {
        g.drawLine(x1, y1, x2, y2);
        g.drawLine(x1-1, y1, x2-1, y2);
        g.drawLine(x1+1, y1, x2+1, y2);
    }

    private void setPointPosition(DrawableDataRow point) {
        int index = this.drawPoints.indexOf(point);
        point.x = this.xMargin + this.xScale*index;
        point.y = (int)(this.yScale*Math.abs(this.maxY.getAmount() - point.getAmount()) + this.yMargin);
    }
    
    private void setLineColor(DrawableDataRow firstPoint, DrawableDataRow secondPoint, Graphics g) {
        g.setColor(Color.CYAN);
    }
    
    private void paintLegend(Graphics g) {
        g.setColor(Color.GRAY);
        g.drawString(this.maxY.getAmount() + "", 50, this.yMargin);
        g.drawString("0", 40, this.zeroPosition);
        g.drawString(this.minY.getAmount() + "", 40, this.yMargin + this.heigth);
        g.setColor(Color.BLACK);
        for(int i = this.yMargin; i < this.heigth + this.yMargin; i += 20) {
            g.drawLine(this.xMargin - 3, i, this.xMargin + 3, i);
        }        
    }
    
    private void regenerate() {
        if (this.points.isEmpty()) {
            return;
        }
        int newWidth = this.points.size() * this.xScale;
        int newHeight = this.getHeight() - this.yMargin*2;
        if (this.width != newWidth || this.heigth != newHeight) {
            this.width = newWidth;
            this.heigth = newHeight;
            this.maxY = GetMaxYRow();
            this.minY = GetMinYRow();
            this.yScale = this.heigth/(Math.abs(this.maxY.getAmount() - this.minY.getAmount()));
            this.zeroPosition = this.yMargin + (int)(this.yScale*this.maxY.getAmount());
            this.setSize(this.width+this.xMargin*2, this.heigth+this.yMargin*2);
            this.setPreferredSize(new Dimension(this.width+this.xMargin*2, this.heigth+this.yMargin*2));
        }    
    }
    
    private DataRow GetMaxYRow() {
        this.points.sort(new Comparator<DataRow>() {
            public int compare(DataRow a, DataRow b) {
                a.setCompareByDate(false);
                int result = a.compareTo(b);
                a.setCompareByDate(true);
                return result;
            }
        });
        return this.points.get(this.points.size() - 1);
    }
    
    private DataRow GetMinYRow() {
        this.points.sort(new Comparator<DataRow>() {
            public int compare(DataRow a, DataRow b) {
                a.setCompareByDate(false);
                int result = a.compareTo(b);
                a.setCompareByDate(true);
                return result;
            }
        });
        return this.points.get(0);
    }
    
    private ArrayList<DrawableDataRow> prepareDataRows(ArrayList<DataRow> points) {
        ArrayList<DrawableDataRow> result = new ArrayList<DrawableDataRow>();
        Iterator<DataRow> i = points.iterator();
        while (i.hasNext()) {
            result.add(new DrawableDataRow(i.next()));
        }
        result.sort(new Comparator<DrawableDataRow>() {

            @Override
            public int compare(DrawableDataRow o1, DrawableDataRow o2) {
                return o1.compareTo(o2);
            }
        });
        return result;
    }
    
    public void SetSelected(int x, int y) {
        Graphics g = this.getGraphics();
        if(this.selectedPoint != null) {
            this.selectedPoint.setNotSelected(g);
        }
        for (int i = 0; i  < this.drawPoints.size(); i++) {
            DrawableDataRow current = this.drawPoints.get(i);
            if (current.setSelected(x, y, g)) {               
                this.selectedPoint = current;
                break;
            }
        }
    }
}
